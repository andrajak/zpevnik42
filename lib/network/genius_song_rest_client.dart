import 'dart:convert';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:html/parser.dart' as html;

import 'package:zpevnik/network/base_rest_client.dart';

class GeniusSongRestClient extends BaseRestClient {
  static GeniusSongRestClient get to => Get.find();

  Future<List<Map<String, dynamic>>?> searchSong(Map<String, dynamic> options) async {
    try {
      _checkOptions(options);
      final String apiKey = options['apiKey'];
      final String title = options['title'];
      final String artist = options['artist'];
      final bool optimizeQuery = options['optimizeQuery'] ?? false;
      final bool authHeader = options['authHeader'] ?? false;

      final String song = optimizeQuery ? _getTitle(title, artist) : '$title $artist';
      final String reqUrl = ApiUrl.endpointSearch + Uri.encodeComponent(song);
      final Map<String, String> headers = {
        'Authorization': 'Bearer $apiKey'
      };

      final response = await http.get(
        authHeader ? Uri.parse(reqUrl) : Uri.parse('$reqUrl&access_token=$apiKey'),
        headers: authHeader ? headers : null,
      );

      if (response.statusCode == 200) {
        final Map<String, dynamic> data = jsonDecode(response.body);
        if (data['response']['hits'].isEmpty) return null;

        final List<Map<String, dynamic>> results = data['response']['hits'].map<Map<String, dynamic>>((val) {
          final String fullTitle = val['result']['full_title'];
          final String songArtImageUrl = val['result']['song_art_image_url'];
          final int id = val['result']['id'];
          final String url = val['result']['url'];
          return {'idG': id, 'title': fullTitle, 'albumArt': songArtImageUrl, 'url': url};
        }).toList();
        return results;
      } else {
        throw Exception('Failed to search song: ${response.statusCode}');
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<String> fetchGeniusLyrics(String url) async {
    try {
      final response = await http.get(Uri.parse(url));
      if (response.statusCode == 200) {
        final document = html.parse(response.body);
        String lyrics = document.querySelector('div.lyrics')?.text.trim() ?? '';
        if (lyrics.isEmpty) {
          lyrics = '';
          document.querySelectorAll('div[class^="Lyrics__Container"]').forEach((element) {
            final snippet = element.innerHtml
                .replaceAll(RegExp('<br>'), '\n')
                .replaceAll(RegExp('<[^>]+>'), '');
            lyrics += Uri.decodeComponent(snippet).trim() + '\n\n';
          });
        }
        if (lyrics.isEmpty) return "";
        return lyrics.trim();
      } else {
        throw Exception('Failed to load page: ${response.statusCode}');
      }
    } catch (e) {
      throw e;
    }
  }

  void _checkOptions(Map<String, dynamic> options) {
    final apiKey = options['apiKey'];
    final title = options['title'];
    final artist = options['artist'];

    if (apiKey == null) {
      throw Exception('"apiKey" property is missing from options');
    } else if (title == null) {
      throw Exception('"title" property is missing from options');
    } else if (artist == null) {
      throw Exception('"artist" property is missing from options');
    }
  }

  String _getTitle(String title, String artist) {
    return '${title} ${artist}'
        .toLowerCase()
        .replaceAll(RegExp(r' *\([^)]*\) *'), '')
        .replaceAll(RegExp(r' *\[[^\]]*]'), '')
        .replaceAll(RegExp(r'feat\.|ft\.'), '')
        .replaceAll(RegExp(r'\s+'), ' ')
        .trim();
  }
}

/// Base info k rest api -> https://docs.genius.com
class ApiUrl {
  static const baseUrl = "https://api.genius.com";

  static const endpointSearch = "$baseUrl/search?per_page=50&q="; // &page=0
}