import 'package:get/get.dart';

import 'package:zpevnik/model/song.dart';
import 'package:zpevnik/network/genius_song_rest_client.dart';

class GeniusSongService extends GetxService {
  static GeniusSongService get to => Get.find();

  static const String apiKey = 'ezXGlkXj0sA7GkhXneRTDlwiqYXXBvFI-dfyK_r0NLCFgPUo6mRZ5-KeThFBwtQB';

  Future<List<Song>> searchSongs(String input) async {
    final Map<String, dynamic> options = {
      'apiKey': apiKey,
      'title': input,
      'artist': '',
      'optimizeQuery': true,
    };

    try {
      List<Song> songs = [];
      final List<Map<String, dynamic>>? result = await GeniusSongRestClient.to.searchSong(options);
      if (result != null) {
        result.forEach((song) {
          songs.add(Song.fromJson(song));
        });
      }
      return songs;
    } catch (e) {
      print('Error: $e');
      return [];
    }
  }

  Future<String> getLyrics(String url) async {
    if (url.isEmpty) return '';
    try {
      final String lyrics = await GeniusSongRestClient.to.fetchGeniusLyrics(url);
      return lyrics;
    } catch (e) {
      print('Error: $e');
      return '';
    }
  }
}
