import 'dart:convert';

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

const String themeModeKey = "themeModeKey";

class SharedPreferencesService extends GetxService {
  static SharedPreferencesService get to => Get.find();

  SharedPreferences? _prefsInstance;

  Future<SharedPreferences> get getPrefsInstance async {
    return _prefsInstance ??= await SharedPreferences.getInstance();
  }

  Future<bool> setBool({bool? value, required String key}) async {
    if (value == null) {
      return remove(key);
    }
    return getPrefsInstance.then((SharedPreferences prefs) => prefs.setBool(key, value));
  }

  Future<bool> setDouble({double? value, required String key}) async {
    if (value == null) {
      return remove(key);
    }
    return getPrefsInstance.then((SharedPreferences prefs) => prefs.setDouble(key, value));
  }

  Future<bool> setInt({int? value, required String key}) async {
    if (value == null) {
      return remove(key);
    }
    return getPrefsInstance.then((SharedPreferences prefs) => prefs.setInt(key, value));
  }

  Future<bool> setString({String? value, required String key}) async {
    if (value == null) {
      return remove(key);
    }
    return getPrefsInstance.then((SharedPreferences prefs) => prefs.setString(key, value));
  }

  Future<bool> remove(String key) async {
    return getPrefsInstance.then((SharedPreferences prefs) => prefs.remove(key));
  }

  Future<bool> containsKey(String key) async {
    return getPrefsInstance.then((SharedPreferences prefs) => prefs.containsKey(key));
  }

  Future<String?> getString({required String key}) async {
    return getPrefsInstance.then((prefs) => prefs.getString(key));
  }

  Future<bool?> getBool({required String key}) async {
    return getPrefsInstance.then((prefs) => prefs.getBool(key));
  }

  Future<double?> getDouble({required String key}) async {
    return getPrefsInstance.then((prefs) => prefs.getDouble(key));
  }

  Future<int?> getInt({required String key}) async {
    return getPrefsInstance.then((prefs) => prefs.getInt(key));
  }
}