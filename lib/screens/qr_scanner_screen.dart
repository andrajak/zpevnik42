import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import 'package:zpevnik/controller/base_controller.dart';
import 'package:zpevnik/controller/song_contorller.dart';
import 'package:zpevnik/generated/locale_keys.g.dart';
import 'package:zpevnik/libraries/easy_localization/lib/src/public.dart';
import 'package:zpevnik/model/song.dart';

class QrScannerScreen extends GetView<_QrScannerController> {
  const QrScannerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<_QrScannerController>(
        init: _QrScannerController(),
        builder: (_QrScannerController controller) {
          return Scaffold(
            appBar: AppBar(
              title: Text(tr(LocaleKeys.add_song_by_qr_code_headline)),
            ),
            body: QRView(
              key: controller.qrKey,
              onQRViewCreated: controller.onQRViewCreated,
              overlay: QrScannerOverlayShape(
                borderColor: Get.theme.colorScheme.secondary,
                borderRadius: 10,
                borderLength: 30,
                borderWidth: 10,
                cutOutSize: 300,
              ),
            ),
          );
        });
  }
}

class _QrScannerController extends BaseController {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  QRViewController? qrController;

  @override
  void onClose() {
    qrController?.dispose();
    super.onClose();
  }

  void onQRViewCreated(QRViewController controller) {
    qrController = controller;
    controller.scannedDataStream.listen((scanData) async {
      final String code = scanData.code ?? tr(LocaleKeys.no_data);

      if (_isValidQRCode(code)) {
        await controller.pauseCamera();
        Navigator.pop(Get.context!, code);
        SongController.to.createSongFromQrCode(code);
        Get.back();
      } else {
        print('Invalid QR Code data: $code');
      }
    });
  }

  bool _isValidQRCode(String code) {
    return code.contains(Song.qrCodeDelimiter) && code.length > Song.qrCodeDelimiter.length;
  }
}
