import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:toastification/toastification.dart';

import 'package:zpevnik/controller/base_controller.dart';
import 'package:zpevnik/controller/song_contorller.dart';
import 'package:zpevnik/network/genius_song_rest_client.dart';
import 'package:zpevnik/screens/settings_screen.dart';
import 'package:zpevnik/screens/song_detail_screen.dart';
import 'package:zpevnik/model/song.dart';
import 'package:zpevnik/service/genius_song_service.dart';
import 'package:zpevnik/generated/locale_keys.g.dart';
import 'package:zpevnik/libraries/easy_localization/lib/src/public.dart';

class SearchScreen extends GetView<_SearchController> {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GetBuilder<_SearchController>(
            init: _SearchController(),
            builder: (_SearchController controller) {
              return Obx(() {
                return Scaffold(
                  body: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 32, 8, 8),
                        child: Row(
                          children: [
                            Expanded(
                              child: TextField(
                                controller: controller.searchController,
                                decoration: InputDecoration(
                                  hintText: tr(LocaleKeys.search_song),
                                ),
                                onChanged: (value) {
                                  controller.updateSearchText(value);
                                },
                                onSubmitted: (value) {
                                  FocusScope.of(context).unfocus();
                                  if (controller.isValidSearchInput(value)) {
                                    controller.onTapSearch(context);
                                  } else {
                                    controller._showInvalidInputDialog(context);
                                  }
                                },
                              ),
                            ),
                            const SizedBox(width: 16),
                            ElevatedButton(
                              onPressed: () {
                                FocusScope.of(context).unfocus();
                                if (controller.isValidSearchInput(controller.searchText.value)) {
                                  controller.onTapSearch(context);
                                } else {
                                  controller._showInvalidInputDialog(context);
                                }
                              },
                              child: Text(tr(LocaleKeys.search)),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: controller.isLoading.value
                            ? controller.progressWidget
                            : Scrollbar(
                                thickness: 2,
                                trackVisibility: true,
                                controller: controller.scrollController,
                                child: ListView.builder(
                                  padding: const EdgeInsets.symmetric(vertical: 0),
                                  itemCount: controller.songs.length,
                                  itemBuilder: (context, index) {
                                    final item = controller.songs[index];
                                    final SimpleSong? simpleSong = SongController.to.splitGeniusSongName(item.title ?? '');
                                    return ListTile(
                                      contentPadding: const EdgeInsets.symmetric(horizontal: 8.0),
                                      title: Text(simpleSong?.name ?? '', style: Get.textTheme.bodyLarge),
                                      subtitle: Text(simpleSong?.author ?? ''),
                                      trailing: Container(
                                        alignment: Alignment.centerRight,
                                        width: 40,
                                        child: PopupMenuButton<String>(
                                            padding: EdgeInsets.zero,
                                            itemBuilder: (context) => [
                                                  PopupMenuItem(
                                                    value: 'save',
                                                    child: ListTile(
                                                      leading: const Icon(Icons.save),
                                                      title: Text(tr(LocaleKeys.save)),
                                                    ),
                                                  ),
                                                ],
                                            onSelected: (value) {
                                              switch (value) {
                                                case 'save':
                                                  controller.onTapDownloadSong(context, controller.songs[index]);
                                              }
                                            }),
                                      ),
                                      onTap: () => controller.onTapSong(context, controller.songs[index]),
                                    );
                                  },
                                ),
                              ),
                      ),
                    ],
                  ),
                );
              });
            }));
  }
}

class _SearchController extends BaseController {
  final ScrollController scrollController = ScrollController();
  final TextEditingController searchController = TextEditingController();

  RxList songs = [].obs;
  RxString searchText = ''.obs;
  RxBool isLoading = false.obs;

  @override
  Future<void> onInit() async {
    super.onInit();
    await GeniusSongRestClient.to.checkInternetConnection();
  }

  @override
  void dispose() {
    searchController.dispose();
    scrollController.dispose();
    super.dispose();
  }

  void updateSearchText(String text) {
    searchText.value = text;
  }

  bool isValidSearchInput(String input) {
    final RegExp validCharacters = RegExp(r'^[a-zA-Z0-9\s]+$');
    return input.length >= 2 && validCharacters.hasMatch(input);
  }


  Future<void> onTapSearch(BuildContext context) async {
    if (hasInternet()) {
      isLoading.value = true;
      songs.clear();
      songs.value = await GeniusSongService.to.searchSongs(searchText.value);
      isLoading.value = false;
    } else {
      _showNoConnectionDialog(context);
    }
  }

  void onTapSong(BuildContext context, Song song) {
    if (hasInternet()) {
      Get.to(() => SongDetailScreen(song: song));
    } else {
      _showNoConnectionDialog(context);
    }
  }

  void onTapSettings() {
    Get.to(() => const SettingsScreen());
  }

  Future<void> onTapDownloadSong(BuildContext context, Song song) async {
    if (hasInternet()) {
      Get.back();
      song.lyrics = await GeniusSongService.to.getLyrics(song.url ?? '');
      SongController.to.createSong(song);
      _showCustomToast(tr(LocaleKeys.saved_successfully + "!"), context);
    } else {
      _showNoConnectionDialog(context);
    }
  }

  void _showCustomToast(String message, BuildContext context) {
    toastification.showCustom(
      context: context,
      autoCloseDuration: const Duration(seconds: 2),
      alignment: Alignment.bottomCenter,
      builder: (BuildContext context, ToastificationItem holder) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.black,
                border: Border.all(color: Colors.red, width: 2),
              ),
              width: 220,
              padding: const EdgeInsets.all(16),
              margin: const EdgeInsets.all(8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(Icons.done),
                  const SizedBox(height: 8),
                  Text(message, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  void _showNoConnectionDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(tr(LocaleKeys.api_error_no_internet), style: const TextStyle(fontWeight: FontWeight.bold)),
          content: Text(tr(LocaleKeys.api_error_no_internet_message)),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('OK'),
            ),
          ],
        );
      },
    );
  }

  void _showInvalidInputDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(tr(LocaleKeys.invalid_input), style: const TextStyle(fontWeight: FontWeight.bold)),
          content: Text(tr(LocaleKeys.invalid_input_message)),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('OK'),
            ),
          ],
        );
      },
    );
  }
}
