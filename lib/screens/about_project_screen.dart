import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:zpevnik/controller/base_controller.dart';
import 'package:zpevnik/generated/locale_keys.g.dart';
import 'package:zpevnik/libraries/easy_localization/lib/src/public.dart';

class AboutProjectScreen extends GetView<_AboutProjectController> {
  const AboutProjectScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(tr(LocaleKeys.about_app)),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Get.back(); // Navigate back to the previous screen
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Text(
            tr(LocaleKeys.about_app_content),
            style: Get.textTheme.bodyLarge,
          ),
        ),
      ),
    );
  }
}

class _AboutProjectController extends BaseController {
  //static _AboutProjectController get to => Get.find();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void dispose() {
    super.dispose();
  }
}
