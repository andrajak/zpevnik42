import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:zpevnik/controller/base_controller.dart';
import 'package:zpevnik/screens/settings_screen.dart';
import 'package:zpevnik/model/song.dart';
import 'package:zpevnik/generated/locale_keys.g.dart';
import 'package:zpevnik/libraries/easy_localization/lib/src/public.dart';

class HomeScreen extends GetView<_HomeController> {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(tr(LocaleKeys.song_book)),
        actions: [
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () => controller.onTapSettings(),
          ),
        ],
      ),
      body: GetBuilder<_HomeController>(
        init: _HomeController(),
        builder: (_HomeController controller) {
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: tr(LocaleKeys.search_song),
                  ),
                  onChanged: (value) {
                    // Handle search text changed
                  },
                ),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: controller.songs.length,
                  itemBuilder: (context, index) {
                    final item = controller.songs[index];
                    return ListTile(
                      leading: const Icon(Icons.music_note),
                      title: Text(item.title ?? ''),
                      trailing: PopupMenuButton(
                        itemBuilder: (context) => [
                          const PopupMenuItem(
                            child: Text('ToDo'),
                            value: 'todo',
                          ),
                        ],
                        onSelected: (value) {
                          // Handle selected option
                        },
                      ),
                      onTap: () => controller.onTapSong(controller.songs[index]),
                    );
                  },
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}

class _HomeController extends BaseController {
  List<Song> songs = [];

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void onTapSettings() {
    Get.to(() => const SettingsScreen());
  }

  void onTapSong(Song song) {
    //Get.to(() => SongDetailScreen(song: song));
  }
}
