import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:toastification/toastification.dart';

import 'package:zpevnik/controller/base_controller.dart';
import 'package:zpevnik/controller/playlist_song_controller.dart';
import 'package:zpevnik/controller/song_contorller.dart';
import 'package:zpevnik/model/song.dart';
import 'package:zpevnik/controller/playlist_controller.dart';
import 'package:zpevnik/model/playlist.dart';
import 'package:zpevnik/service/session_manager.dart';
import 'package:zpevnik/app_theme.dart';
import 'package:zpevnik/generated/locale_keys.g.dart';
import 'package:zpevnik/libraries/easy_localization/lib/src/public.dart';
import 'package:zpevnik/service/genius_song_service.dart';

class SongDetailScreen extends GetView<_SongDetailController> {
  final Song song;
  final bool isLocalSong;

  const SongDetailScreen({Key? key, required this.song, this.isLocalSong = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<_SongDetailController>(
        init: _SongDetailController(selectedSong: song, isLocalSong: isLocalSong),
        builder: (_SongDetailController controller) {
          return Obx(() {
            final songs = SongController.to.songs.toList();
            final SimpleSong? simpleSong = SongController.to.splitGeniusSongName(song.title ?? '');

            return Scaffold(
              appBar: AppBar(
                  title: Text(simpleSong?.name ?? ''),
                  leading: IconButton(
                    icon: const Icon(Icons.arrow_back),
                    onPressed: () {
                      Get.back();
                    },
                  ),
                  actions: [
                    isLocalSong
                        ? const SizedBox()
                        : IconButton(
                            icon: const Icon(Icons.save),
                            onPressed: () => controller.onTapDownloadSong(context, song),
                          ),
                    isLocalSong
                        ? IconButton(
                            icon: const Icon(Icons.playlist_add),
                            onPressed: () {
                              _showBottomSheet(context, 2);
                            },
                          )
                        : const SizedBox(),
                    PopupMenuButton<String>(
                      itemBuilder: (context) => [
                        PopupMenuItem(
                          value: 'qrGenerate',
                          child: ListTile(leading: Icon(Icons.qr_code), title: Text(tr(LocaleKeys.share_by_qr_code))),
                        ),
                      ],
                      onSelected: (value) {
                        switch (value) {
                          case 'qrGenerate':
                            showDialog(
                              context: context,
                              builder: (context) {
                                return Dialog(
                                  insetPadding: const EdgeInsets.all(12),
                                  backgroundColor: Colors.white,
                                  child: SizedBox(
                                    width: 400,
                                    height: 405,
                                    child: Center(
                                      child: Column(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(20),
                                            child: Text(tr(LocaleKeys.qr_code_for_song_share), style: Get.textTheme.headlineMedium?.copyWith(color: Colors.black)),
                                          ),
                                          QrImageView(
                                            data: "${song.title ?? ""}${Song.qrCodeDelimiter}${song.lyrics ?? ""}",
                                            version: QrVersions.auto,
                                            size: 320,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              },
                            );
                            break;
                        }
                      },
                    ),
                  ]),
              body: controller.isDownloaded.value
                  ? SingleChildScrollView(
                      controller: controller.scrollController,
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(controller.selectedSong.lyrics ?? "", style: Get.textTheme.bodyMedium?.copyWith(fontSize: SessionManager.to.lyricsFontSize)),
                          ],
                        ),
                      ),
                    )
                  : controller.progressWidget,
              bottomNavigationBar: BottomAppBar(
                height: 69,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    IconButton(
                      icon: const Icon(Icons.display_settings),
                      onPressed: () => _showBottomSheet(context, 0),
                    ),
                    IconButton(
                      icon: const Icon(Icons.headphones),
                      onPressed: () => _showBottomSheet(context, 1),
                    ),
                    IconButton(
                      icon: const Icon(Icons.fullscreen),
                      onPressed: () => controller.onTapFullscreen(),
                    ),
                    const Spacer(),
                    controller.isPlaying.value
                        ? Row(children: [
                            IconButton(
                              icon: const Icon(Icons.remove, color: AppTheme.accentColor),
                              onPressed: () => controller.onTapSlowerScroll(),
                            ),
                            IconButton(
                              icon: const Icon(Icons.add, color: AppTheme.accentColor),
                              onPressed: () => controller.onTapFasterScroll(),
                            ),
                            IconButton(
                              icon: const Icon(Icons.stop, color: AppTheme.accentColor),
                              onPressed: () => controller.onTapStopScroll(),
                            ),
                          ])
                        : IconButton(
                            icon: const Icon(Icons.arrow_downward, color: AppTheme.accentColor),
                            onPressed: () => controller.onTapStartScroll(),
                          ),
                  ],
                ),
              ),
            );
          });
        });
  }

  void _showBottomSheet(BuildContext context, int index) {
    Widget bottomSheetContent = Container();
    switch (index) {
      case 0:
        bottomSheetContent = _buildSettingsBottomSheet();
        break;
      case 1:
        bottomSheetContent = _buildPlayAudioBottomSheet();
        break;
      case 2:
        bottomSheetContent = _buildAddToPlaylistBottomSheet();
        break;
    }

    showModalBottomSheet(
      context: context,
      builder: (context) => bottomSheetContent,
    );
  }

  Widget _buildSettingsBottomSheet() {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            title: Text(tr(LocaleKeys.song_settings)),
          ),
        ],
      ),
    );
  }

  Widget _buildPlayAudioBottomSheet() {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            title: Text(tr(LocaleKeys.play_audio)),
          ),
        ],
      ),
    );
  }

  Widget _buildAddToPlaylistBottomSheet() {
    final double bottomSheetHeight = 85 + (48 * controller.playlists.length).toDouble();
    const double bottomSheetHeightMax = 360;

    return Container(
      padding: EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            title: Text(tr(LocaleKeys.playlists), style: Get.textTheme.bodyLarge?.copyWith(fontSize: 18)),
            subtitle: Text(tr(LocaleKeys.select_playlist_or_create_new)),
          ),
          SizedBox(
            height: bottomSheetHeight < bottomSheetHeightMax ? bottomSheetHeight : bottomSheetHeightMax,
            child: ListView(
              children: [
                ListTile(
                  contentPadding: const EdgeInsets.symmetric(horizontal: 6.0),
                  leading: const Icon(Icons.add),
                  title: Text(
                    tr(LocaleKeys.new_playlist),
                    style: Get.textTheme.bodyLarge,
                  ),
                  onTap: () {},
                ),
                const Divider(
                  thickness: 1,
                ),
                ...controller.playlists.map((playlist) {
                  return Container(
                    height: 48,
                    alignment: Alignment.centerLeft,
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: ListTile(
                      contentPadding: EdgeInsets.zero,
                      leading: const Icon(Icons.playlist_play_rounded),
                      title: Text(
                        playlist.name ?? "",
                        style: Get.textTheme.bodyLarge,
                      ),
                      onTap: () => controller.onTapAddToPlaylist(playlist),
                    ),
                  );
                }).toList(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _SongDetailController extends BaseController {
  final Song selectedSong;
  final bool isLocalSong;
  late List<Playlist> playlists;
  Rx<bool> isDownloaded = false.obs;

  ScrollController scrollController = ScrollController();
  Timer? scrollTimer;
  double scrollSpeed = 3;
  RxBool isPlaying = false.obs;

  _SongDetailController({required this.selectedSong, required this.isLocalSong});

  @override
  Future<void> onInit() async {
    super.onInit();
    if (!isLocalSong) {
      selectedSong.lyrics = await GeniusSongService.to.getLyrics(selectedSong.url ?? '');
    }
    isDownloaded.value = true;
    playlists = PlaylistController.to.playlists.toList();
  }

  @override
  void onClose() {
    scrollTimer?.cancel();
    scrollController.dispose();
    super.onClose();
  }

  void onTapStartScroll() {
    if (isDownloaded.value == false) {
      return;
    }
    isPlaying.value = true;
    scrollTimer = Timer.periodic(const Duration(milliseconds: 100), (timer) {
      double offset = scrollController.offset + (scrollSpeed);
      if (scrollController.hasClients) {
        if (offset >= scrollController.position.maxScrollExtent) {
          onTapStopScroll();
        } else {
          scrollController.animateTo(
            offset,
            duration: const Duration(milliseconds: 100),
            curve: Curves.linear,
          );
        }
      }
    });
  }

  void onTapStopScroll() {
    isPlaying.value = false;
    scrollTimer?.cancel();
  }

  void onTapSlowerScroll() {
    scrollSpeed = (scrollSpeed - 1).clamp(1, 5);
  }

  void onTapFasterScroll() {
    scrollSpeed = (scrollSpeed + 1).clamp(1, 5);
  }

  void onTapFullscreen() {}

  void onTapDownloadSong(BuildContext context, Song song) {
    if (isDownloaded.value) {
      SongController.to.createSong(song);
      _showCustomToast(tr(LocaleKeys.saved_successfully) + "!", context);
    }
  }

  void onTapAddToPlaylist(Playlist playlist) {
    if (isDownloaded.value) {
      PlaylistSongController.to.createPlaylistSong(playlist.id ?? 0, selectedSong.id ?? 0);
      Get.back();
    }
  }

  void _showCustomToast(String message, BuildContext context) {
    toastification.showCustom(
      context: context,
      autoCloseDuration: const Duration(seconds: 2),
      alignment: Alignment.bottomCenter,
      builder: (BuildContext context, ToastificationItem holder) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.black,
                border: Border.all(color: Colors.red, width: 2),
              ),
              width: 220,
              padding: const EdgeInsets.all(16),
              margin: const EdgeInsets.all(8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(Icons.done),
                  const SizedBox(height: 8),
                  Text(message, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}
