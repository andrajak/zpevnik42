import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:zpevnik/controller/base_controller.dart';
import 'package:zpevnik/model/song.dart';
import 'package:zpevnik/screens/qr_scanner_screen.dart';
import 'package:zpevnik/screens/settings_screen.dart';
import 'package:zpevnik/screens/song_detail_screen.dart';
import 'package:zpevnik/controller/song_contorller.dart';
import 'package:zpevnik/generated/locale_keys.g.dart';
import 'package:zpevnik/libraries/easy_localization/lib/src/public.dart';

class SongListViewScreen extends GetView<_SongListViewController> {
  const SongListViewScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<_SongListViewController>(
      init: _SongListViewController(),
      builder: (_SongListViewController controller) {
        return Obx(() {
          final songs = SongController.to.songs.toList();

          return Scaffold(
            appBar: AppBar(
              title: Text(tr(LocaleKeys.songs)),
              actions: [
                IconButton(
                  padding: const EdgeInsets.only(right: 16.0),
                  icon: const Icon(Icons.qr_code_scanner),
                  onPressed: () => controller.onTapQrScannerScreen(),
                ),
                IconButton(
                  padding: const EdgeInsets.only(right: 16.0),
                  icon: const Icon(Icons.settings),
                  onPressed: () => controller.onTapSettings(),
                ),
              ],
            ),
            body: songs.isNotEmpty
                ? Scrollbar(
                    thickness: 2,
                    trackVisibility: true,
                    controller: controller.scrollController,
                    child: ListView.builder(
                      itemCount: songs.length,
                      itemBuilder: (context, index) {
                        final song = songs[index];
                        final SimpleSong? simpleSong = SongController.to.splitGeniusSongName(song.title ?? '');
                        return ListTile(
                          contentPadding: const EdgeInsets.symmetric(horizontal: 8.0),
                          title: Text(simpleSong?.name ?? '', style: Get.textTheme.bodyLarge),
                          subtitle: Text(simpleSong?.author ?? ''),
                          trailing: Container(
                            alignment: Alignment.centerRight,
                            width: 40,
                            child: PopupMenuButton<String>(
                              itemBuilder: (context) => [
                                PopupMenuItem(
                                  value: 'delete',
                                  child: Text(tr(LocaleKeys.delete)),
                                ),
                              ],
                              onSelected: (value) {
                                switch (value) {
                                  case 'delete':
                                    controller.onTapDeleteSong(song);
                                    break;
                                }
                              },
                            ),
                          ),
                          onTap: () => controller.onTapSong(song),
                        );
                      },
                    ),
                  )
                : Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(tr(LocaleKeys.add_new_song_by_search_and_download)),
                        Text(tr(LocaleKeys.or)),
                        Text(tr(LocaleKeys.new_song_by_scan_qr_code)),
                      ],
                    ),
                  ),
          );
        });
      },
    );
  }
}

class _SongListViewController extends BaseController {
  final ScrollController scrollController = ScrollController();
  final TextEditingController songNameController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void onTapDeleteSong(Song song) {
    SongController.to.deleteSongWithDependencies(song);
  }

  void onTapUpdateSong(Song song) {
    SongController.to.updateSong(song);
  }

  void onTapSong(Song song) {
    Get.to(() => SongDetailScreen(song: song, isLocalSong: true,));
  }

  void onTapQrScannerScreen() {
    Get.to(() => const QrScannerScreen());
  }

  void onTapSettings() {
    Get.to(() => const SettingsScreen());
  }
}
