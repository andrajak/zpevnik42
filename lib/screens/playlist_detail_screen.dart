import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:zpevnik/controller/base_controller.dart';
import 'package:zpevnik/controller/playlist_song_controller.dart';
import 'package:zpevnik/model/playlist.dart';
import 'package:zpevnik/screens/song_detail_screen.dart';
import 'package:zpevnik/controller/song_contorller.dart';
import 'package:zpevnik/model/song.dart';
import 'package:zpevnik/generated/locale_keys.g.dart';
import 'package:zpevnik/libraries/easy_localization/lib/src/public.dart';

class PlaylistDetailScreen extends GetView<_PlaylistDetailController> {
  final Playlist selectedPlaylist;

  const PlaylistDetailScreen({Key? key, required this.selectedPlaylist}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(selectedPlaylist.name ?? ""),
      ),
      body: GetBuilder<_PlaylistDetailController>(
        init: _PlaylistDetailController(selectedPlaylist: selectedPlaylist),
        builder: (_PlaylistDetailController controller) {
          return controller.songs.isNotEmpty
              ? ListView.builder(
                  itemCount: controller.songs.length,
                  itemBuilder: (context, index) {
                    final song = controller.songs[index];
                    final SimpleSong? simpleSong = SongController.to.splitGeniusSongName(song.title ?? '');
                    return ListTile(
                      leading: const Icon(Icons.music_note),
                      title: Text(simpleSong?.name ?? ""),
                      subtitle: Text(simpleSong?.author ?? ""),
                      onTap: () => controller.onTapSong(song),
                    );
                  },
                )
              : Center(
            child: Text(tr(LocaleKeys.add_some_songs_to_playlist)),
          );
        },
      ),
    );
  }
}

class _PlaylistDetailController extends BaseController {
  final Playlist selectedPlaylist;
  late RxList<Song> songs = <Song>[].obs;

  _PlaylistDetailController({required this.selectedPlaylist});

  @override
  void onInit() {
    super.onInit();
    _fetchSongs();
  }

  Future<void> _fetchSongs() async {
    songs.value = await PlaylistSongController.to.findSongsForPlaylist(selectedPlaylist.id ?? 0);
    update();
  }

  void onTapSong(Song song) {
    Get.to(() => SongDetailScreen(song: song, isLocalSong: true));
  }
}
