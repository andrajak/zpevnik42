import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:zpevnik/controller/base_controller.dart';
import 'package:zpevnik/controller/playlist_controller.dart';
import 'package:zpevnik/model/playlist.dart';
import 'package:zpevnik/screens/playlist_detail_screen.dart';
import 'package:zpevnik/screens/settings_screen.dart';
import 'package:zpevnik/generated/locale_keys.g.dart';
import 'package:zpevnik/libraries/easy_localization/lib/src/public.dart';

class PlaylistScreen extends GetView<_PlaylistController> {
  const PlaylistScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<_PlaylistController>(
      init: _PlaylistController(),
      builder: (_PlaylistController controller) {
        return Obx(
          () {
            final playlists = PlaylistController.to.playlists.toList();

            return Scaffold(
              appBar: AppBar(
                title: Text(tr(LocaleKeys.playlists)),
                actions: [
                  IconButton(
                    padding: const EdgeInsets.only(right: 16.0),
                    icon: const Icon(Icons.settings),
                    onPressed: () => controller.onTapSettings(),
                  ),
                ],
              ),
              body: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    child: playlists.isNotEmpty
                        ? Scrollbar(
                            thickness: 2,
                            trackVisibility: true,
                            controller: controller.scrollController,
                            child: ListView.builder(
                              itemCount: playlists.length,
                              itemBuilder: (context, index) {
                                final playlist = playlists[index];
                                return ListTile(
                                  contentPadding: const EdgeInsets.symmetric(horizontal: 8.0),
                                  title: Text(playlist.name ?? "", style: Get.textTheme.bodyLarge),
                                  trailing: Container(
                                    alignment: Alignment.centerRight,
                                    width: 40,
                                    child: PopupMenuButton<String>(
                                      itemBuilder: (context) => [
                                        PopupMenuItem(
                                          value: 'edit',
                                          child: Text(tr(LocaleKeys.edit)),
                                        ),
                                        PopupMenuItem(
                                          value: 'delete',
                                          child: Text(tr(LocaleKeys.delete)),
                                        ),
                                      ],
                                      onSelected: (value) {
                                        switch (value) {
                                          case 'edit':
                                            showEditPlaylistDialog(playlist);
                                            break;
                                          case 'delete':
                                            controller.onTapDeletePlaylist(playlist);
                                            break;
                                        }
                                      },
                                    ),
                                  ),
                                  onTap: () => controller.onTapPlaylistDetail(playlist),
                                );
                              },
                            ),
                          )
                        : Center(
                            child: Text(tr(LocaleKeys.create_new_playlist)),
                          ),
                  ),
                ],
              ),
              floatingActionButton: FloatingActionButton(
                onPressed: () => showCreatePlaylistDialog(),
                child: const Icon(Icons.add),
              ),
            );
          },
        );
      },
    );
  }

  void showEditPlaylistDialog(Playlist playlist) {
    controller.playlistNameController.text = playlist.name ?? '';
    Get.dialog(
      AlertDialog(
        title: Text(tr(LocaleKeys.edit_playlist)),
        content: TextField(
          controller: controller.playlistNameController,
          decoration: InputDecoration(
            hintText: playlist.name,
          ),
        ),
        actions: [
          TextButton(
            onPressed: () {
              Get.back();
            },
            child: Text(tr(LocaleKeys.cancel)),
          ),
          ElevatedButton(
            onPressed: () => controller.onTapUpdatePlaylist(playlist),
            child: Text(tr(LocaleKeys.save)),
          ),
        ],
      ),
    );
  }

  void showCreatePlaylistDialog() {
    Get.dialog(
      AlertDialog(
        title: Text(tr(LocaleKeys.new_playlist)),
        content: TextField(
          controller: controller.playlistNameController,
          decoration: InputDecoration(
            hintText: tr(LocaleKeys.name),
          ),
        ),
        actions: [
          TextButton(
            onPressed: () {
              Get.back();
            },
            child: Text(tr(LocaleKeys.cancel)),
          ),
          ElevatedButton(
            onPressed: () => controller.onTapCreatePlaylist(),
            child: Text(tr(LocaleKeys.create)),
          ),
        ],
      ),
    );
  }
}

class _PlaylistController extends BaseController {
  final ScrollController scrollController = ScrollController();
  final TextEditingController playlistNameController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void dispose() {
    scrollController.dispose();
    playlistNameController.dispose();
    super.dispose();
  }

  void onTapPlaylistDetail(Playlist playlist) {
    Get.to(() => PlaylistDetailScreen(selectedPlaylist: playlist));
  }

  void onTapDeletePlaylist(Playlist playlist) {
    PlaylistController.to.deletePlaylist(playlist);
  }

  void onTapCreatePlaylist() {
    final playlistName = playlistNameController.text;
    if (playlistName.isNotEmpty) {
      PlaylistController.to.createPlaylist(Playlist(name: playlistName));
      Get.back();
      playlistNameController.text = '';
    }
  }

  void onTapUpdatePlaylist(Playlist playlist) {
    final playlistNewName = playlistNameController.text;
    if (playlistNewName.isNotEmpty) {
      final editedPlaylist = playlist;
      editedPlaylist.name = playlistNewName;
      PlaylistController.to.updatePlaylist(playlist);
      Get.back();
      playlistNameController.text = '';
    }
  }

  void onTapSettings() {
    Get.to(() => const SettingsScreen());
  }
}
