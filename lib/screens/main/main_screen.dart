import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:zpevnik/controller/base_controller.dart';
import 'package:zpevnik/generated/locale_keys.g.dart';
import 'package:zpevnik/model/navigation_menu_item.dart';
import 'package:zpevnik/libraries/easy_localization/lib/src/public.dart';
import 'navigation/bottom_navigation_utils.dart';
import 'navigation/named_bottom_navigation_bar_item.dart';

class MainScreen extends GetView<MainScreenController> {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MainScreenController>(
      init: MainScreenController(),
      builder: (controller) {
        return _mobileContent(context);
      },
    );
  }

  Widget _mobileContent(BuildContext context) {
    final theme = Theme.of(context);
    final List<Widget> screens = controller.screens;

    return WillPopScope(
      onWillPop: () async {
        return controller.willPopHandler(context);
      },
      child: Scaffold(
          key: controller._scaffoldKey,
          resizeToAvoidBottomInset: false,
          body: IndexedStack(
            index: controller.currentTabIndex,
            children: screens,
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
          bottomNavigationBar: BottomAppBar(
            height: 69,
            padding: EdgeInsets.zero,
            color: theme.bottomAppBarTheme.surfaceTintColor,
            child: Stack(
              children: [
                bottomNavigationBar(controller, context),
                BottomNavigationBarUtils.bottomNavigationBarIndicator(controller, context),
                // BottomNavigationBarUtils.bottomNavigationBarShadowCover(context),
              ],
            ),
          )),
    );
  }

  Widget bottomNavigationBar(MainScreenController controller, BuildContext context) {
    final theme = Theme.of(context);

    final List<NamedBottomNavigationBarItem> items = [
      //tabItem(LocaleKeys.tab_bar_home, Icon(Platform.isIOS ? Icons.home : Icons.home)), //const ImageIcon(AssetImage('assets/images/ic_24_home.png'))
      tabItem(tr(LocaleKeys.search_headline), Icon(Platform.isIOS ? Icons.search : Icons.search)),
      tabItem(tr(LocaleKeys.downloaded), Icon(Platform.isIOS ? Icons.music_note : Icons.music_note)),
      tabItem(tr(LocaleKeys.playlists), Icon(Platform.isIOS ? Icons.playlist_play_rounded : Icons.playlist_play_rounded)),
    ];

    controller.namedNavigationItems.assignAll(items);

    return BottomNavigationBar(
      elevation: 1,
      iconSize: 30,
      items: items,
      currentIndex: controller.currentTabIndex,
      backgroundColor: theme.bottomAppBarColor,
      selectedItemColor: theme.colorScheme.secondary,
      unselectedItemColor: theme.hintColor,
      selectedLabelStyle: theme.textTheme.titleSmall?.copyWith(fontSize: 13),
      unselectedLabelStyle: theme.textTheme.titleSmall?.copyWith(fontSize: 12),
      onTap: (int index) => controller.onBottomNavigationTap(items[index], index),
      type: BottomNavigationBarType.fixed,
    );
  }
}

class MainScreenController extends BaseController {
  static MainScreenController get to => Get.find();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  int currentTabIndex = 0;
  RxList<NavigationMenuItem> menuItems = RxList<NavigationMenuItem>();
  RxList<NamedBottomNavigationBarItem> namedNavigationItems = RxList<NamedBottomNavigationBarItem>();
  List<Widget> screens = [];
  DateTime? dateTimeWhenPaused;

  MainScreenController() {
    setMenuItems();
  }

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  Future<void> onReady() async {
    super.onReady();

    setMenuItems();
    setTabIndexOf(NavigationMenuItemType.search);
  }

  @override
  void onPaused() {
    dateTimeWhenPaused = DateTime.now();
    super.onPaused();
  }

  @override
  void onResumed() {
    super.onResumed();
    dateTimeWhenPaused = null;
  }

  void setMenuItems() {
    menuItems.value = NavigationMenu.itemsForMobile();
    screens = menuItems.map((e) => e.screen).toList();
  }

  bool willPopHandler(BuildContext context) {
    final menuItem = menuItems[currentTabIndex];
    if (menuItem.type == NavigationMenuItemType.home) {
      return true;
    }
    setTabIndexOf(NavigationMenuItemType.home);
    return false;
  }

  void onBottomNavigationTap(NamedBottomNavigationBarItem tappedItem, int index) {
    setTabIndex(index);
  }

  void setTabIndexOf(NavigationMenuItemType menuType, {int? innerPageTabIndex}) {
    final index = menuItems.indexWhere((item) => item.type == menuType);
    if (index == -1) {
      assert(false);
      return;
    }
    setTabIndex(index, innerPageTabIndex: innerPageTabIndex);
  }

  void setTabIndex(int index, {int? innerPageTabIndex}) {
    if (menuItems.isEmpty) {
      return;
    }

    if (menuItems.length <= index) {
      index = 0;
      assert(false);
    }
    final menuItem = menuItems[index];

    if (menuItem.onTap != null) {
      menuItem.onTap!();
      return;
    }

    currentTabIndex = index;

    update();
  }
}
