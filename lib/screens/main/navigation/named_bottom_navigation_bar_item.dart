import 'package:flutter/material.dart';

class NamedBottomNavigationBarItem extends BottomNavigationBarItem {
  final String analyticsLogName;

  NamedBottomNavigationBarItem({
    required Widget icon,
    required String label,
    required this.analyticsLogName,
  })  : super(icon: icon, label: label);
}
