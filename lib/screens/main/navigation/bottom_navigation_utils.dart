import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:zpevnik/libraries/easy_localization/lib/src/public.dart';
import 'package:zpevnik/screens/main/main_screen.dart';
import 'named_bottom_navigation_bar_item.dart';

class BottomNavigationBarUtils {
  static Widget bottomNavigationBarIndicator(MainScreenController controller, BuildContext context) {
    final theme = Theme.of(context);
    final indicatorWidth = Get.mediaQuery.size.width / controller.menuItems.length;
    final indicatorStart = indicatorWidth * controller.currentTabIndex;
    return Positioned(
      top: 0,
      left: indicatorStart,
      child: Container(
        width: indicatorWidth,
        height: 2,
        decoration: BoxDecoration(
          color: theme.colorScheme.secondary,
        ),
      ),
    );
  }

  static Widget bottomNavigationBarShadowCover(BuildContext context) {
    final theme = Theme.of(context);
    final indicatorWidth = Get.mediaQuery.size.width;
    return Positioned(
      bottom: 0,
      left: 0,
      child: Container(
        width: indicatorWidth,
        height: 10,
        decoration: BoxDecoration(
          color: theme.bottomAppBarColor,
        ),
      ),
    );
  }
}

NamedBottomNavigationBarItem tabItem(String label, Widget icon) {
  return NamedBottomNavigationBarItem(
    icon: Padding(
      padding: const EdgeInsets.all(4),
      child: icon,
    ),
    label: tr(label),
    analyticsLogName: label,
  );
}
