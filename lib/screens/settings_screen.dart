import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:zpevnik/controller/base_controller.dart';
import 'package:zpevnik/generated/locale_keys.g.dart';
import 'package:zpevnik/screens/about_project_screen.dart';

import '../libraries/easy_localization/lib/src/public.dart';
import '../service/session_manager.dart';
import '../service/shared_preferences_manager.dart';

class SettingsScreen extends GetView<_SettingsController> {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return GetBuilder<_SettingsController>(
        init: _SettingsController(),
        builder: (_SettingsController controller) {
          return Obx(() {
            return Scaffold(
              appBar: AppBar(
                title: Text(tr(LocaleKeys.settings)),
                leading: IconButton(
                  icon: const Icon(Icons.clear),
                  onPressed: () {
                    Get.back(); // Navigate back to the previous screen
                  },
                ),
              ),
              body: ListView(
                children: [
                  _buildThemeSwitch(context, theme),
                  _buildShowNotesSwitch(context, theme),
                  _buildFontSizeSlider(context, theme),
                  _buildAboutProject(context, theme),
                ],
              ),
            );
          });
        });
  }

  Widget _buildThemeSwitch(BuildContext context, ThemeData theme) {
    return ListTile(
      title: Text(tr(LocaleKeys.dark_theme), style: Get.textTheme.bodyLarge),
      trailing: SizedBox(
        width: 48,
        child: Switch(
          activeColor: theme.colorScheme.secondary,
          activeTrackColor: theme.colorScheme.secondary.withOpacity(0.5),
          value: SessionManager.to.themeModeIndex.value != ThemeMode.light,
          onChanged: (value) {
            controller.toggleTheme(value);
          },
        ),
      ),
    );
  }

  Widget _buildShowNotesSwitch(BuildContext context, ThemeData theme) {
    return ListTile(
      title: Text(tr(LocaleKeys.show_chords), style: Get.textTheme.bodyLarge),
      trailing: SizedBox(
        width: 48,
        child: Switch(
          activeColor: theme.colorScheme.secondary,
          activeTrackColor: theme.colorScheme.secondary.withOpacity(0.5),
          value: controller.showNotes.value,
          onChanged: (value) {
            controller.toggleShowNotes(value);
          },
        ),
      ),
    );
  }

  Widget _buildFontSizeSlider(BuildContext context, ThemeData theme) {
    return SizedBox(
      height: 60,
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Text(tr(LocaleKeys.font_size), style: Get.textTheme.bodyLarge),
          ),
          const Spacer(),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Text('A', style: Get.textTheme.bodyMedium?.copyWith(fontSize: SessionManager.to.lyricsFontSizeMin)),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 0.0),
            child: Slider(
              value: SessionManager.to.lyricsFontSize,
              min: SessionManager.to.lyricsFontSizeMin,
              max: SessionManager.to.lyricsFontSizeMax,
              divisions: 13,
              onChanged: (value) {
                controller.updateFontSize(value);
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 32),
            child: Text('A', style: Get.textTheme.bodyMedium?.copyWith(fontSize: SessionManager.to.lyricsFontSizeMax)),
          ),
        ],
      ),
    );
  }

  Widget _buildAboutProject(BuildContext context, ThemeData theme) {
    return ListTile(
      title: Center(
        child: GestureDetector(
          child: Text(tr(LocaleKeys.about_app), style: Get.textTheme.bodyMedium?.copyWith(color: theme.hintColor)),
          onTap: () => controller.onTapAboutProject(),
        ),
      ),
    );
  }
}

class _SettingsController extends BaseController {
  Rx<bool> showNotes = false.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void toggleTheme(bool value) {
    value ? setTheme(ThemeMode.dark) : setTheme(ThemeMode.light);
  }

  void setTheme(ThemeMode themeMode) {
    SessionManager.to.themeModeIndex.value = themeMode;
    SharedPreferencesService.to.setInt(value: themeMode.index, key: themeModeKey);
    update();
  }

  void toggleShowNotes(bool value) {
    showNotes.value = value;
  }

  void updateFontSize(double value) {
    SessionManager.to.lyricsFontSize = value;
    update();
  }

  void onTapAboutProject() {
    Get.to(() => const AboutProjectScreen());
  }
}
