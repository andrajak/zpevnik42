import 'package:flutter/cupertino.dart';

class AutoFit extends StatelessWidget {
  final Widget? child;
  final AlignmentGeometry alignment;
  final bool boundChildWidth;

  const AutoFit({
    Key? key,
    this.child,
    this.alignment = Alignment.centerLeft,
    this.boundChildWidth = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constrains) {
      return FittedBox(
        fit: BoxFit.scaleDown,
        alignment: alignment,
        child: boundChildWidth
            ? SizedBox(
          width: constrains.maxWidth,
          child: child,
        )
            : child,
      );
    });
  }
}
