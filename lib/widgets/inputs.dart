import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:zpevnik/generated/locale_keys.g.dart';
import 'package:zpevnik/libraries/easy_localization/lib/src/public.dart';
import 'package:zpevnik/service/session_manager.dart';

enum TextFieldButtonType {
  none,
  clear,
  eye,
}

class ZTextField extends StatefulWidget {
  final String? label;
  final String? hint;
  final String? initialValue;
  final String? suffixText;
  final bool obscureText;
  final bool useInnerLabel;
  final bool fixedSizeWithValidator;
  final FormFieldSetter<String>? onChanged;
  final FormFieldSetter<String>? onSaved;
  final VoidCallback? onSubmitted;
  final bool autoFocus;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final double height;
  late final TextEditingController controller;
  final bool isEnabled;
  final TextFieldButtonType buttonType;
  final FormFieldValidator<String>? validator;
  final int? maxLength;
  final TextCapitalization textCapitalization;
  final AutovalidateMode autovalidateMode;
  final List<FilteringTextInputFormatter> inputFormatters;
  final double fillOpacity;
  final int maxLines;
  final Color? fillColor;
  final List<String>? autofillHints;
  final VoidCallback? onEditingComplete;
  final FocusScopeNode? focusScopeNode;
  final Widget? keyboardActionsWidget;
  final RxBool _forceShowText = false.obs;
  final ScrollController scrollController = ScrollController();
  final bool? scrollTextToStartOnFocusLost;
  final bool fixedWidth;

  ZTextField({
    Key? key,
    this.label,
    this.hint,
    this.initialValue,
    this.obscureText = false,
    this.useInnerLabel = false,
    this.fixedSizeWithValidator = true,
    this.onChanged,
    this.onSaved,
    this.onSubmitted,
    this.keyboardType = TextInputType.text,
    this.textInputAction = TextInputAction.next,
    this.autoFocus = false,
    this.height = 59,
    this.autovalidateMode = AutovalidateMode.onUserInteraction,
    this.isEnabled = true,
    this.buttonType = TextFieldButtonType.clear,
    this.validator,
    this.maxLength,
    this.textCapitalization = TextCapitalization.sentences,
    this.suffixText,
    this.inputFormatters = const <FilteringTextInputFormatter>[],
    this.fillOpacity = 1.0,
    this.maxLines = 1,
    this.fillColor,
    this.autofillHints,
    this.onEditingComplete,
    this.focusScopeNode,
    this.keyboardActionsWidget,
    this.scrollTextToStartOnFocusLost,
    TextEditingController? controller,
    this.fixedWidth = true,
  }) : super(key: key) {
    this.controller = controller ?? TextEditingController();
  }

  @override
  _ZTextFieldState createState() => _ZTextFieldState();
}

class _ZTextFieldState extends State<ZTextField> {
  bool _showButton = false;
  final FocusNode focusNode = FocusNode(
      canRequestFocus: false,
      onKeyEvent: (FocusNode node, KeyEvent event) {
        return KeyEventResult.ignored;
      });

  final FocusNode innerFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();

    Future.microtask(() {
      _updateControllerValue();
      _updateClearButtonVisibility();
      widget.controller.addListener(() {
        _updateClearButtonVisibility();
      });
      innerFocusNode.addListener(() {
        _updateClearButtonVisibility();

        if (widget.scrollTextToStartOnFocusLost ?? false) {
          _scrollToStartOnFocusLost();
        }

        if (innerFocusNode.hasFocus) {
          widget.controller.text = widget.controller.text.replaceAll("\t", "");
        }
      });
    });
  }

  void _scrollToStartOnFocusLost() {
    if (!innerFocusNode.hasFocus) {
      widget.scrollController.animateTo(0, duration: const Duration(milliseconds: 200), curve: Curves.linear);
    }
  }

  @override
  void didUpdateWidget(oldWidget) {
    widget._forceShowText.value = oldWidget._forceShowText.value;
    super.didUpdateWidget(oldWidget);
  }

  void _updateControllerValue() {
    if (widget.controller.text.isEmpty) //
      widget.controller.text = widget.initialValue ?? "";
  }

  void _updateClearButtonVisibility() {
    if (!mounted) //
      return;
    if (widget.buttonType != TextFieldButtonType.none && widget.suffixText == null) {
      setState(() {
        _showButton = widget.controller.text.isNotEmpty && focusNode.hasFocus;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final double minHeight = widget.height + (widget.validator != null && widget.fixedSizeWithValidator ? 26 : 0);
    final double maxWidth = widget.fixedWidth ? Get.width : double.infinity;
    final double minWidth = widget.fixedWidth ? Get.width : 0;
    return RawKeyboardListener(
      focusNode: focusNode,
      autofocus: false,
      child: Container(
        constraints: BoxConstraints(minHeight: minHeight, maxWidth: maxWidth, minWidth: minWidth),
        child: textFormField,
      ),
      onKey: (RawKeyEvent event) {
        if (Platform.isIOS) {
          return;
        }

        if (event.data is RawKeyEventDataAndroid) {
          final RawKeyEventDataAndroid androidEventData = event.data as RawKeyEventDataAndroid;
          if (androidEventData.deviceId == -1) //
            return;
        }

        final bool isKeyUp = event.runtimeType == RawKeyUpEvent;

        if (isKeyUp && event.logicalKey == LogicalKeyboardKey.enter) {
          _moveToNextFocus();
        }
      },
    );
  }

  String get iconPathForButton {
    switch (widget.buttonType) {
      case TextFieldButtonType.none:
        return "";
      case TextFieldButtonType.clear:
        return "assets/images/ic_24_clear.png";
      case TextFieldButtonType.eye:
        final obscure = widget.obscureText && widget._forceShowText.isFalse;
        return obscure ? "assets/images/ic_24_see.png" : "assets/images/ic_24_unsee.png";
    }
  }

  Widget get textFormField => Obx(
        () {
          final double verticalPadding = max((widget.height - 14) / 2, 0);
          final bool isDarkMode = SessionManager.to.isDarkMode;
          final Color enabledBorderColor = isDarkMode ? Theme.of(context).hintColor : Theme.of(context).dividerColor;
          final obscure = widget.obscureText && widget._forceShowText.isFalse;
          return TextFormField(
            scrollController: widget.scrollController,
            maxLines: widget.maxLines,
            obscureText: obscure,
            controller: widget.controller,
            keyboardType: widget.keyboardType,
            onChanged: widget.onChanged,
            onSaved: widget.onSaved,
            textInputAction: widget.textInputAction,
            autofocus: widget.autoFocus,
            enabled: widget.isEnabled,
            textCapitalization: widget.textCapitalization,
            autovalidateMode: widget.autovalidateMode,
            inputFormatters: widget.inputFormatters,
            focusNode: innerFocusNode,
            maxLength: widget.maxLength,
            validator: (String? value) => widget.validator?.call(value),
            autofillHints: widget.autofillHints,
            onEditingComplete: () {
              TextInput.finishAutofillContext();
              if (widget.controller.text.isNotEmpty) {
                _moveToNextFocus();
              }
            },
            onFieldSubmitted: (_) {
              if (widget.controller.text.isNotEmpty) {
                _moveToNextFocus(allowSubmit: false);
              }
            },
            style: Theme.of(context).textTheme.headlineMedium,
            textAlignVertical: TextAlignVertical.center,
            decoration: InputDecoration(
              isCollapsed: true,
              counterText: '',
              contentPadding: EdgeInsets.only(left: 16, right: _showButton ? 0 : 16, top: verticalPadding, bottom: verticalPadding),
              fillColor: widget.fillColor ?? Theme.of(context).backgroundColor.withOpacity(widget.fillOpacity),
              errorMaxLines: 15,
              errorStyle: const TextStyle(fontSize: 12, color: Color(0xFFDD2F2F)),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).colorScheme.secondary, width: 2),
                borderRadius: const BorderRadius.all(Radius.circular(4)),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: enabledBorderColor),
                borderRadius: const BorderRadius.all(Radius.circular(4)),
              ),
              disabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
              ),
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).dividerColor),
                borderRadius: const BorderRadius.all(Radius.circular(4)),
              ),
              alignLabelWithHint: true,
              labelStyle: TextStyle(color: Theme.of(context).hintColor, fontWeight: FontWeight.w500),
              labelText: widget.label,
              hintText: widget.hint,
              suffixIconConstraints: const BoxConstraints(maxHeight: 48, maxWidth: 48),
              suffixText: widget.suffixText,
              suffixIcon: _showButton
                  ? IconButton(
                      autofocus: false,
                      icon: Image.asset(
                        iconPathForButton,
                        color: isDarkMode ? Colors.grey : Colors.black54,
                        width: 24,
                        excludeFromSemantics: true,
                      ),
                      onPressed: () {
                        switch (widget.buttonType) {
                          case TextFieldButtonType.none:
                            return;
                          case TextFieldButtonType.clear:
                            widget.controller.clear();
                            widget.onChanged?.call(null);
                            break;
                          case TextFieldButtonType.eye:
                            widget._forceShowText.toggle();
                            break;
                        }
                        focusNode.requestFocus();
                      },
                    )
                  : null,
            ),
          );
        },
      );

  void _moveToNextFocus({bool allowSubmit = true}) {
    if (widget.textInputAction == TextInputAction.next) {
      FocusScope.of(context).nextFocus();
    } else if (widget.textInputAction == TextInputAction.done || widget.textInputAction == TextInputAction.go || widget.textInputAction == TextInputAction.search) {
      FocusScope.of(context).unfocus();
      if (allowSubmit) {
        widget.onSubmitted?.call();
      }
    }
  }
}
