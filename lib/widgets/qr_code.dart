import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class CustomQrCode extends StatefulWidget {
  final String? data;
  final double? size;
  final bool readByCamera;

  const CustomQrCode({Key? key, this.data, this.size = 320, this.readByCamera = false}) : super(key: key);

  @override
  _CustomQrCodeState createState() => _CustomQrCodeState();
}

class _CustomQrCodeState extends State<CustomQrCode> {
  @override
  void initState() {
    super.initState();
    if (!widget.readByCamera) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _showQrDialog();
      });
    }
  }

  void _showQrDialog() {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          content: SizedBox(
            width: 300,
            height: 280,
            child: Center(
              child: QrImageView(
                data: widget.data ?? "No data",
                version: QrVersions.auto,
                size: 320,
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return widget.readByCamera ? _readByCameraView(context) : Container();
  }

  Widget _readByCameraView(BuildContext context) {
    return Center(child: Text('QR Code Scanner Placeholder'));
  }
}
