import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'custom_checkbox_list_tile.dart';
import 'custom_radio_list_tile.dart' as custom;
import 'package:zpevnik/libraries/easy_localization/lib/src/public_ext.dart';
import '../generated/locale_keys.g.dart';
import '../service/session_manager.dart';
import 'auto_fit.dart';

class ZButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final String? title;
  final String? subtitle;
  final Widget? titleWidget;
  final Widget? leadingWidget;
  final Color? color;
  final bool transparentBackgroundColor;
  final Color? imageColor;
  final TextStyle? titleStyle;

  final String? imagePath;
  final double? imageWidth;
  final IconData? icon;

  final bool small;
  final bool keepTextColor;
  final bool smallPadding;
  final double? fixedWidth;
  final bool isOutlined;
  final double outlineWidth;
  final bool fullWidth;
  final bool isEnabled;
  final bool clearBackground;
  final bool useOriginalImageColor;
  final bool fillWhenDisabled;
  final String? semanticsLabel;
  final bool selectable;
  final bool isSelected;

  const ZButton(
      this.title,
      this.subtitle,
      this.leadingWidget,
      this.titleWidget,
      this.imagePath,
      this.imageWidth,
      this.icon,
      this.small,
      this.keepTextColor,
      this.smallPadding,
      this.isOutlined,
      this.outlineWidth,
      this.fixedWidth,
      this.onPressed,
      this.fullWidth,
      this.isEnabled,
      this.clearBackground,
      this.color,
      this.transparentBackgroundColor,
      this.imageColor,
      this.titleStyle,
      this.useOriginalImageColor,
      this.fillWhenDisabled,
      this.semanticsLabel, {
        this.selectable = false,
        this.isSelected = false,
      });

  const ZButton.accent({
    String? title,
    String? subtitle,
    Widget? leadingWidget,
    Widget? titleWidget,
    String? imagePath,
    IconData? icon,
    double imageWidth = 24.0,
    bool small = false,
    bool keepTextColor = false,
    bool smallPadding = false,
    double? fixedWidth,
    VoidCallback? onPressed,
    bool fullWidth = true,
    bool isEnabled = true,
    String? semanticsLabel,
  }) : this(
    title,
    subtitle,
    leadingWidget,
    titleWidget,
    imagePath,
    imageWidth,
    icon,
    small,
    keepTextColor,
    smallPadding,
    false,
    2.0,
    fixedWidth,
    onPressed,
    fullWidth,
    isEnabled,
    false,
    null,
    false,
    null,
    null,
    false,
    false,
    semanticsLabel,
  );

  const ZButton.outlined({
    String? title,
    String? subtitle,
    Widget? leadingWidget,
    Widget? titleWidget,
    String? imagePath,
    IconData? icon,
    double imageWidth = 24.0,
    bool small = false,
    bool keepTextColor = false,
    bool smallPadding = false,
    double? fixedWidth,
    VoidCallback? onPressed,
    bool fullWidth = true,
    bool isEnabled = true,
    bool clearBackground = false,
    String? semanticsLabel,
    bool? isSelected,
  }) : this(
    title,
    subtitle,
    leadingWidget,
    titleWidget,
    imagePath,
    imageWidth,
    icon,
    small,
    keepTextColor,
    smallPadding,
    true,
    2,
    fixedWidth,
    onPressed,
    fullWidth,
    isEnabled,
    clearBackground,
    null,
    false,
    null,
    null,
    false,
    false,
    semanticsLabel,
    selectable: isSelected != null,
    isSelected: isSelected ?? false,
  );

  const ZButton.simple({
    String? title,
    String? subtitle,
    Widget? leadingWidget,
    Widget? titleWidget,
    String? imagePath,
    IconData? icon,
    double imageWidth = 24.0,
    bool small = false,
    bool keepTextColor = false,
    bool smallPadding = false,
    double? fixedWidth,
    VoidCallback? onPressed,
    Color? color,
    bool transparentBackgroundColor = false,
    TextStyle? titleStyle,
    bool fullWidth = true,
    bool isEnabled = true,
    bool clearBackground = true,
    String? semanticsLabel,
  }) : this(
    title,
    subtitle,
    leadingWidget,
    titleWidget,
    imagePath,
    imageWidth,
    icon,
    small,
    keepTextColor,
    smallPadding,
    false,
    2.0,
    fixedWidth,
    onPressed,
    fullWidth,
    isEnabled,
    clearBackground,
    color,
    transparentBackgroundColor,
    null,
    titleStyle,
    false,
    false,
    semanticsLabel,
  );

  const ZButton.dynamic({
    String? title,
    String? subtitle,
    Widget? leadingWidget,
    Widget? titleWidget,
    required bool isOutlined,
    double outlineWidth = 2.0,
    String? imagePath,
    IconData? icon,
    double imageWidth = 24.0,
    bool small = false,
    bool keepTextColor = false,
    bool smallPadding = false,
    double? fixedWidth,
    VoidCallback? onPressed,
    bool fullWidth = true,
    bool isEnabled = true,
    String? semanticsLabel,
  }) : this(
    title,
    subtitle,
    leadingWidget,
    titleWidget,
    imagePath,
    imageWidth,
    icon,
    small,
    keepTextColor,
    smallPadding,
    isOutlined,
    outlineWidth,
    fixedWidth,
    onPressed,
    fullWidth,
    isEnabled,
    false,
    null,
    false,
    null,
    null,
    false,
    false,
    semanticsLabel,
  );

  const ZButton.custom({
    String? title,
    String? subtitle,
    Widget? leadingWidget,
    Widget? titleWidget,
    Color? color,
    bool transparentBackgroundColor = false,
    Color? imageColor,
    bool useOriginalImageColor = false,
    String? imagePath,
    IconData? icon,
    double imageWidth = 24.0,
    bool small = false,
    bool keepTextColor = false,
    bool smallPadding = false,
    bool isOutlined = false,
    double outlineWidth = 2.0,
    double? fixedWidth,
    VoidCallback? onPressed,
    bool fullWidth = true,
    bool isEnabled = true,
    bool fillWhenDisabled = false,
    String? semanticsLabel,
  }) : this(
    title,
    subtitle,
    leadingWidget,
    titleWidget,
    imagePath,
    imageWidth,
    icon,
    small,
    keepTextColor,
    smallPadding,
    isOutlined,
    outlineWidth,
    fixedWidth,
    onPressed,
    fullWidth,
    isEnabled,
    false,
    color,
    transparentBackgroundColor,
    imageColor,
    null,
    useOriginalImageColor,
    fillWhenDisabled,
    semanticsLabel,
  );

  @override
  Widget build(BuildContext context) {
    final bool isEnabled = this.isEnabled && onPressed != null;
    final ThemeData theme = Theme.of(context);
    Color backgroundColor = theme.backgroundColor;
    if (!clearBackground) {
      backgroundColor = isOutlined ? theme.cardColor : theme.colorScheme.secondary;
    }
    if (color != null) {
      backgroundColor = color!;
    }
    if (transparentBackgroundColor) {
      backgroundColor = MediaQuery.of(context).platformBrightness == Brightness.dark ? Colors.transparent : theme.cardColor;
    }
    if (isSelected) {
      backgroundColor = theme.colorScheme.secondary.withAlpha(13);
    }

    final bool isSmallScreen = MediaQuery.of(context).size.height < 600;
    final bool hasSubtitle = subtitle?.isNotEmpty ?? false;
    double height = small ? 40.0 : 56.0;
    if (isSmallScreen) {
      height += 10;
    }
    final Color? textColor = isOutlined
        ? theme.textTheme.bodyLarge?.color
        : transparentBackgroundColor
        ? theme.colorScheme.primary
        : Colors.white;
    final Color? borderColor = isEnabled
        ? isSelected
        ? theme.colorScheme.secondary
        : theme.textTheme.bodyLarge?.color
        : theme.colorScheme.onSurface;
    final bool hasImage = imagePath != null || icon != null;

    final List<Widget> widgets = [];

    if (title?.isNotEmpty ?? false) {
      final List<Widget> titleWidgets = <Widget>[
        Text(
          title!,
          maxLines: hasSubtitle ? 1 : 3,
          softWrap: true,
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.center,
          style: small && !keepTextColor ? theme.textTheme.titleMedium?.copyWith(color: textColor) : titleStyle,
        )
      ];

      if (hasSubtitle) {
        titleWidgets.add(Padding(
          padding: EdgeInsets.only(top: isSmallScreen ? 0 : 4),
          child: FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              subtitle!,
              maxLines: 1,
              textAlign: TextAlign.center,
              style: theme.textTheme.titleSmall?.copyWith(color: textColor, fontWeight: FontWeight.w400),
            ),
          ),
        ));
      }
      if (titleWidgets.length > 1) {
        widgets.add(Container(
          padding: EdgeInsets.only(left: hasImage ? 32 : 0, right: hasImage ? imageWidth ?? 0 : 0),
          height: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: titleWidgets,
          ),
        ));
      } else {
        widgets.add(Padding(
          padding: EdgeInsets.only(left: hasImage ? 32 : 0, right: hasImage ? imageWidth ?? 0 : 0),
          child: titleWidgets.first,
        ));
      }
    } else if (titleWidget != null) {
      widgets.add(
        Padding(padding: EdgeInsets.only(left: hasImage ? 32 : 0, right: hasImage ? imageWidth ?? 0 : 0), child: titleWidget),
      );
    }

    if (imagePath != null) {
      Color? imageColor;
      if (isEnabled) {
        imageColor = useOriginalImageColor ? null : this.imageColor ?? textColor;
      } else {
        imageColor = theme.dividerColor;
      }
      widgets.add(Positioned(left: 0, child: Image.asset(imagePath!, width: imageWidth, color: imageColor)));
    } else if (icon != null) {
      widgets.add(Positioned(left: 0, child: Icon(icon!, size: imageWidth)));
    } else if (leadingWidget != null) {
      widgets.add(Positioned(left: 0, child: leadingWidget!));
    }

    return RawKeyboardListener(
      focusNode: FocusNode(),
      child: Obx(
            () {
          final bool isDarkMode = SessionManager.to.isDarkMode;
          return Semantics(
            enabled: isEnabled,
            button: true,
            label: semanticsLabel ?? "${title ?? ""} ${subtitle ?? ""}",
            child: ExcludeSemantics(
              child: MaterialButton(
                onPressed: isEnabled ? onPressed : null,
                padding: smallPadding ? const EdgeInsets.symmetric(horizontal: 16) : null,
                height: height,
                elevation: 0,
                disabledColor: (isOutlined && !fillWhenDisabled)
                    ? theme.backgroundColor
                    : isDarkMode
                    ? theme.cardColor
                    : theme.canvasColor,
                disabledTextColor: (isOutlined && !fillWhenDisabled) ? theme.disabledColor : theme.dividerColor,
                color: backgroundColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4),
                  side: (isOutlined && (!fillWhenDisabled || isEnabled) && borderColor != null) //
                      ? BorderSide(width: outlineWidth, color: borderColor)
                      : BorderSide.none,
                ),
                child: SizedBox(
                  width: fullWidth ? double.infinity : fixedWidth,
                  height: height,
                  child: Stack(
                    alignment: Alignment.center,
                    children: widgets,
                  ),
                ),
              ),
            ),
          );
        },
      ),
      onKey: (RawKeyEvent event) {
        if (Platform.isIOS) {
          return;
        }
        if (event.data is RawKeyEventDataAndroid) {
          final RawKeyEventDataAndroid androidEventData = event.data as RawKeyEventDataAndroid;
          if (androidEventData.deviceId == -1) //
            return;
        }

        final bool isKeyUp = event.runtimeType.toString() == 'RawKeyUpEvent';

        if (isKeyUp) {
          if (isEnabled && event.data.logicalKey == LogicalKeyboardKey.enter) {
            onPressed?.call();
          } else if (event.data.logicalKey == LogicalKeyboardKey.tab) {
            FocusScope.of(context).nextFocus();
          }
        }
      },
    );
  }
}

class MyQPopUpButton extends StatelessWidget {
  final List<MyQPopUpItem> items;
  final Color? iconColor;
  final EdgeInsets padding;
  final AlignmentGeometry alignment;
  final Widget? icon;
  final VoidCallback? onPopupInvoke;
  final String? semanticsLabel;

  MyQPopUpButton({
    required this.items,
    this.iconColor,
    this.icon,
    this.padding = const EdgeInsets.all(8.0),
    this.alignment = Alignment.center,
    this.onPopupInvoke,
    this.semanticsLabel,
  });

  final GlobalKey<PopupMenuButtonState<MyQPopUpItem>> _key = GlobalKey();

  Icon get defaultIcon => Icon(
    Platform.isIOS ? Icons.more_horiz : Icons.more_vert,
    color: iconColor ?? Theme.of(Get.context!).hintColor,
  );

  @override
  Widget build(BuildContext context) {
    return Semantics(
      label: semanticsLabel,
      button: true,
      child: ExcludeSemantics(
        child: Platform.isIOS //
            ? _cupertinoButton(context)
            : _materialButton(),
      ),
    );
  }

  void showButtonMenu(BuildContext context) {
    if (Platform.isIOS) {
      onPopupInvoke?.call();
      showCupertinoModalPopup(context: context, builder: (BuildContext context) => _cupertinoActionSheet(context: context));
    } else {
      _key.currentState?.showButtonMenu();
    }
  }

  Widget _cupertinoButton(BuildContext context) {
    return IconButton(
      alignment: alignment,
      padding: padding,
      icon: icon ?? defaultIcon,
      tooltip: semanticsLabel,
      onPressed: () => showButtonMenu(context),
    );
  }

  Widget _cupertinoActionSheet({required BuildContext context}) {
    final List<CupertinoActionSheetAction> actions = items
        .map((MyQPopUpItem item) => CupertinoActionSheetAction(
      child: Text(item.title),
      isDestructiveAction: item.isDestructive,
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop(false);
        item.onSelected?.call();
      },
    ))
        .toList();

    return CupertinoActionSheet(
      actions: actions,
      cancelButton: CupertinoActionSheetAction(
        child: const Text(LocaleKeys.cancel).tr(),
        isDestructiveAction: false,
        isDefaultAction: true,
        onPressed: () {
          Navigator.of(context, rootNavigator: true).pop(false);
        },
      ),
    );
  }

  Widget _materialButton() {
    final List<PopupMenuItem<MyQPopUpItem>> menuItems = items
        .map((MyQPopUpItem item) => PopupMenuItem<MyQPopUpItem>(
      child: Text(item.title, style: Get.textTheme.titleMedium),
      value: item,
    ))
        .toList();

    return PopupMenuButton<MyQPopUpItem>(
      key: _key,
      padding: padding,
      icon: icon ?? defaultIcon,
      tooltip: semanticsLabel,
      onSelected: (MyQPopUpItem result) {
        result.onSelected?.call();
      },
      itemBuilder: (BuildContext context) {
        onPopupInvoke?.call();
        return menuItems;
      },
    );
  }
}

class MyQPopUpItem {
  final String title;
  final Icon? icon;
  final VoidCallback? onSelected;
  final bool isDestructive;

  MyQPopUpItem({
    required this.title,
    this.icon,
    this.onSelected,
    this.isDestructive = false,
  });
}

class MyQListTile<T> extends StatelessWidget {
  final TileType type;
  final String? title;
  final Widget? titleWidget;
  final Widget? subtitleWidget;
  final bool isSelected;
  final T? optionValue;
  final T? groupValue;
  final Function(T?)? onChanged;
  final EdgeInsets? insets;
  final bool smallSubtitle;
  final Widget? trailing;
  final String? semanticLabel;

  const MyQListTile(
      this.type,
      this.title,
      this.titleWidget,
      this.subtitleWidget,
      this.isSelected,
      this.optionValue,
      this.groupValue,
      this.onChanged,
      this.insets,
      this.smallSubtitle,
      this.trailing,
      this.semanticLabel,
      );

  const MyQListTile.radio({
    String? title,
    Widget? titleWidget,
    Widget? subtitleWidget,
    T? optionValue,
    T? groupValue,
    Function(T?)? onChanged,
    EdgeInsets? insets,
    bool smallSubtitle = false,
    Widget? trailing,
    String? semanticLabel,
  }) : this(TileType.radio, title, titleWidget, subtitleWidget, false, optionValue, groupValue, onChanged, insets, smallSubtitle, trailing, semanticLabel);

  const MyQListTile.checkBox({
    String? title,
    Widget? titleWidget,
    Widget? subtitleWidget,
    bool isSelected = true,
    Function(T?)? onChanged,
    EdgeInsets? insets,
    bool smallSubtitle = false,
    Widget? trailing,
    String? semanticLabel,
    bool disableCheckBox = false,
  }) : this(disableCheckBox ? TileType.checkboxDisabled : TileType.checkbox, title, titleWidget, subtitleWidget, isSelected, null, null, onChanged, insets, smallSubtitle, trailing, semanticLabel);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final maxLines = (title?.contains(" ") ?? false) ? 2 : 1;

    final Widget titleWidget = this.titleWidget ??
        AutoFit(
          boundChildWidth: maxLines != 1,
          child: Text(
            title ?? "",
            style: theme.textTheme.headlineMedium?.copyWith(
              color: onChanged != null ? theme.colorScheme.primary : theme.disabledColor,
            ),
            overflow: TextOverflow.ellipsis,
            maxLines: maxLines,
          ),
        );

    Widget content;

    switch (type) {
      case TileType.checkbox:
        content = _checkBoxTile(titleWidget);
        break;
      case TileType.radio:
        content = _radioTile(titleWidget);
        break;
      case TileType.checkboxDisabled:
        content = _checkBoxTile(titleWidget, disableCheckBox: true);
        break;
      default:
        return Container();
    }

    return RawKeyboardListener(
        focusNode: FocusNode(),
        child: content,
        onKey: (RawKeyEvent event) {
          if (Platform.isIOS) {
            return;
          }

          if (event.data is RawKeyEventDataAndroid) {
            final RawKeyEventDataAndroid androidEventData = event.data as RawKeyEventDataAndroid;
            if (androidEventData.deviceId == -1) //
              return;
          }

          final bool isKeyUp = event.runtimeType.toString() == 'RawKeyUpEvent';

          if (isKeyUp) {
            if (event.logicalKey == LogicalKeyboardKey.enter && onChanged != null) {
              if (type == TileType.checkbox) {
                onChanged!(!isSelected as T);
              } else if (optionValue != null) {
                onChanged!(optionValue!);
              }
            } else if (event.logicalKey == LogicalKeyboardKey.tab) {
              FocusScope.of(context).nextFocus();
            }
          }
        });
  }

  Widget _checkBoxTile(Widget titleWidget, {bool disableCheckBox = false}) {
    return Semantics(
      hint: semanticLabel,
      child: Theme(
        data: Get.theme.copyWith(
          splashColor: disableCheckBox ? Colors.transparent : null,
          highlightColor: disableCheckBox ? Colors.transparent : null,
        ),
        child: CustomCheckboxListTile(
          title: Semantics(excludeSemantics: semanticLabel != null, child: titleWidget),
          subtitle: subtitleWidget != null //
              ? FittedBox(
              fit: BoxFit.scaleDown,
              alignment: Alignment.centerLeft,
              child: Semantics(
                excludeSemantics: semanticLabel != null,
                child: subtitleWidget,
              ))
              : null,
          value: isSelected,
          onChanged: (bool? value) => onChanged != null ? onChanged!(value as T) : null,
          contentPadding: insets,
          activeColor: Get.theme.colorScheme.secondary,
          secondary: trailing,
          controlAffinity: ListTileControlAffinity.leading,
          disableCheckBox: disableCheckBox,
        ),
      ),
    );
  }

  Widget _radioTile(Widget titleWidget) {
    return custom.RadioListTile<T>(
      title: titleWidget,
      subtitle: subtitleWidget,
      value: optionValue!,
      onChanged: onChanged!,
      contentPadding: insets,
      groupValue: groupValue!,
      activeColor: Get.theme.colorScheme.secondary,
      controlAffinity: ListTileControlAffinity.leading,
      checkMark: Platform.isIOS
          ? const Padding(
        padding: EdgeInsets.only(left: 0.0),
        child: Icon(Icons.check, color: CupertinoColors.activeBlue),
      )
          : null,
    );
  }
}

enum TileType { checkbox, checkboxDisabled, radio }
