import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpevnik/generated/locale_keys.g.dart';
import 'package:zpevnik/libraries/easy_localization/lib/src/public.dart';
import 'package:zpevnik/network/genius_song_rest_client.dart';
import 'package:zpevnik/service/session_manager.dart';

class CustomNetworkScrollView extends StatelessWidget {
  final Widget? appBar;
  final Widget? removePaddingSliver;
  final List<Widget>? slivers;
  final bool showNetworkSliverByDefault;
  final bool scrollable;
  final ScrollController? controller;

  const CustomNetworkScrollView({
    this.appBar,
    this.removePaddingSliver,
    this.showNetworkSliverByDefault = true,
    this.slivers,
    this.scrollable = false,
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    if (showNetworkSliverByDefault) {
      return content(context);
    }
    return Obx(() => content(context));
  }

  Widget content(BuildContext context) {
    return CustomScrollView(
      physics: scrollable //
          ? Platform.isIOS
          ? const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics())
          : const AlwaysScrollableScrollPhysics()
          : const NeverScrollableScrollPhysics(),
      controller: controller,
      slivers: [
        if (showNetworkSliverByDefault) //
          Obx(() => noInternetSliverAppBar)
        else if (GeniusSongRestClient.to.hasNetworkConnection.isFalse)
          noInternetSliverAppBar,
        if (appBar != null) appBar!,
        if (removePaddingSliver != null)
          MediaQuery.removePadding(
            context: context,
            removeTop: true,
            removeBottom: true,
            child: SliverFillRemaining(
              child: removePaddingSliver!,
            ),
          ),
        ...?slivers,
      ],
    );
  }

  Widget get noInternetSliverAppBar {
    return SliverAppBar(
      titleSpacing: 0.0,
      toolbarHeight: GeniusSongRestClient.to.hasNetworkConnection.isTrue ? 0 : 24,
      pinned: true,
      automaticallyImplyLeading: false,
      title: GeniusSongRestClient.to.hasNetworkConnection.isTrue
          ? const SizedBox()
          : Container(
        color: SessionManager.to.isDarkMode //
            ? const Color(0xff9e9e9e)
            : Colors.black,
        height: 24,
        padding: const EdgeInsets.symmetric(vertical: 4.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset("assets/images/ic_24_wifi.png", width: 16.0),
            const SizedBox(width: 16.0),
            Text(
              tr(LocaleKeys.no_internet_connection_app_bar),
              style: Get.textTheme.bodyLarge?.copyWith(
                fontSize: 12.0,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
