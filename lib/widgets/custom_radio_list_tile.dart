import 'package:flutter/material.dart';

class RadioListTile<T> extends StatelessWidget {
  /// Creates a combination of a list tile and a radio button.
  ///
  /// The radio tile itself does not maintain any state. Instead, when the radio
  /// button is selected, the widget calls the [onChanged] callback. Most
  /// widgets that use a radio button will listen for the [onChanged] callback
  /// and rebuild the radio tile with a new [groupValue] to update the visual
  /// appearance of the radio button.
  ///
  /// The following arguments are required:
  ///
  /// * [value] and [groupValue] together determine whether the radio button is
  ///   selected.
  /// * [onChanged] is called when the user selects this radio button.
  const RadioListTile(
      {Key? key,
        required this.value,
        required this.groupValue,
        required this.onChanged,
        this.toggleable = false,
        this.activeColor,
        this.title,
        this.subtitle,
        this.isThreeLine = false,
        this.dense,
        this.secondary,
        this.selected = false,
        this.controlAffinity = ListTileControlAffinity.platform,
        this.autofocus = false,
        this.contentPadding,
        this.checkMark})
      : assert(toggleable != null),
        assert(isThreeLine != null),
        assert(!isThreeLine || subtitle != null),
        assert(selected != null),
        assert(controlAffinity != null),
        assert(autofocus != null),
        super(key: key);

  final T value;

  final T groupValue;

  final ValueChanged<T?> onChanged;

  final bool toggleable;

  final Color? activeColor;

  final Widget? title;

  final Widget? subtitle;

  final Widget? secondary;

  final bool isThreeLine;

  final bool? dense;

  final bool selected;

  final ListTileControlAffinity controlAffinity;

  final bool autofocus;

  final EdgeInsetsGeometry? contentPadding;

  final Widget? checkMark;

  bool get checked => value == groupValue;

  @override
  Widget build(BuildContext context) {
    Widget? control;
    if (checkMark != null) {
      control = value == groupValue ? checkMark : Container(width: 24);
    } else {
      control = Radio<T>(
        value: value,
        groupValue: groupValue,
        onChanged: onChanged,
        toggleable: toggleable,
        activeColor: activeColor,
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        autofocus: autofocus,
      );
    }

    Widget? leading, trailing;
    switch (controlAffinity) {
      case ListTileControlAffinity.leading:
      case ListTileControlAffinity.platform:
        leading = control;
        trailing = secondary;
        break;
      case ListTileControlAffinity.trailing:
        leading = secondary;
        trailing = control;
        break;
    }
    return MergeSemantics(
      child: ListTileTheme.merge(
        selectedColor: activeColor ?? Theme.of(context).colorScheme.secondary,
        child: ListTile(
          leading: leading,
          title: title,
          subtitle: subtitle,
          trailing: trailing,
          isThreeLine: isThreeLine,
          dense: dense,
          enabled: onChanged != null,
          onTap: onChanged != null
              ? () {
            if (toggleable && checked) {
              onChanged(null);
              return;
            }
            if (!checked) {
              onChanged(value);
            }
          }
              : null,
          selected: selected,
          autofocus: autofocus,
          contentPadding: contentPadding,
        ),
      ),
    );
  }
}
