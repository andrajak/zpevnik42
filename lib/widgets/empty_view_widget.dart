import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpevnik/libraries/easy_localization/lib/src/public_ext.dart';

import 'package:zpevnik/generated/locale_keys.g.dart';
import 'package:zpevnik/libraries/easy_localization/lib/src/public.dart';
import 'package:zpevnik/network/genius_song_rest_client.dart';
import 'package:zpevnik/service/session_manager.dart';
import 'package:zpevnik/widgets/z_button.dart';

class EmptyViewWidget extends StatelessWidget {
  final String? title;
  final String? message;
  final String? imagePath;
  final VoidCallback? buttonAction;
  final VoidCallback? tryAgainAction;
  final String? buttonTitle;
  final bool flexibleBessie;

  const EmptyViewWidget({
    this.title,
    this.message,
    this.imagePath,
    this.buttonAction,
    this.buttonTitle,
    this.tryAgainAction,
    this.flexibleBessie = false,
  });

  @override
  Widget build(BuildContext context) {
    String? title = this.title;
    String? message = this.message;
    VoidCallback? action = buttonAction;
    String? buttonTitle = this.buttonTitle;
    String? imagePath = this.imagePath;

    final TextTheme textTheme = Get.textTheme;
    if (!GeniusSongRestClient.to.hasNetworkConnection.value) {
      title = LocaleKeys.api_error_no_internet;
      message = LocaleKeys.api_error_no_internet_message;
      action = tryAgainAction;
      buttonTitle = LocaleKeys.try_again;
      imagePath = null;
    }

    return Obx(() {
      return Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 24.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                tr(title ?? LocaleKeys.empty_view_title),
                style: textTheme.headlineMedium,
                textAlign: TextAlign.center,
              ),
              if (message?.isNotEmpty ?? false) ...<Widget>[
                const SizedBox(height: 12.0),
                Text(
                  tr(message!),
                  style: textTheme.bodyMedium,
                  textAlign: TextAlign.center,
                ),
              ],
              if (action != null) ...<Widget>[
                const SizedBox(height: 16.0),
                ZButton.accent(
                  title: tr(buttonTitle ?? LocaleKeys.try_again),
                  onPressed: action,
                  small: true,
                  fullWidth: false,
                ),
              ],
            ],
          ),
        ),
      );
    });
  }
}

class ArrowEmptyWidget extends StatelessWidget {
  final String textKey;
  final double? bottomPadding;

  const ArrowEmptyWidget({required this.textKey, this.bottomPadding});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 32.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (MediaQuery.of(context).size.shortestSide > 600)
                const Flexible(
                  child: SizedBox(),
                ),
              Flexible(
                child: Text(
                  textKey,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headlineMedium,
                ).tr(),
              ),
            ],
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.only(bottom: 30 + (bottomPadding ?? 0), right: 90.0),
              child: Obx(
                    () => Image.asset(
                  "assets/images/arrow_top_to_right.png",
                  height: 90,
                  color: SessionManager.to.isDarkMode ? Colors.white : Colors.black,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
