import 'package:get/get.dart';

import 'package:zpevnik/controller/playlist_controller.dart';
import 'package:zpevnik/controller/song_contorller.dart';
import 'package:zpevnik/screens/settings_screen.dart';
import 'package:zpevnik/service/session_manager.dart';
import 'package:zpevnik/service/genius_song_service.dart';
import 'package:zpevnik/network/genius_song_rest_client.dart';
import 'package:zpevnik/controller/playlist_song_controller.dart';
import 'package:zpevnik/database/database.dart';
import 'package:zpevnik/service/shared_preferences_manager.dart';

Future<void> setupServices() async {
  final AppDatabase db = await Get.putAsync(() => $FloorAppDatabase
      .databaseBuilder(AppDatabase.databaseName)
      .build());

  /// Inicializace singleton GetxService - jednodušší nalezení controlleru v paměti;
  Get.lazyPut<SharedPreferencesService>(() => SharedPreferencesService());
  Get.lazyPut<SessionManager>(() => SessionManager());
  Get.lazyPut<GeniusSongRestClient>(() => GeniusSongRestClient());
  Get.lazyPut<GeniusSongService>(() => GeniusSongService());

  /// Inicializace singleton GetxController -> permanent: true -> zamezí smazání z paměti
  Get.put(PlaylistController(database: db), permanent: true);
  Get.put(SongController(database: db), permanent: true);
  Get.put(PlaylistSongController(database: db), permanent: true);
}
