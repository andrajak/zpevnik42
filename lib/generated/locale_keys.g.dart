// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const cancel = 'cancel';
  static const api_error_generic = 'api_error_generic';
  static const api_error_generic_message = 'api_error_generic_message';
  static const api_error_no_internet = 'api_error_no_internet';
  static const api_error_no_internet_message = 'api_error_no_internet_message';
  static const api_error_timeout = 'api_error_timeout';
  static const api_error_timeout_message = 'api_error_timeout_message';
  static const try_again = 'try_again';
  static const empty_not_allowed = 'empty_not_allowed';
  static const empty_view_title = 'empty_view_title';
  static const reload = 'reload';
  static const no_internet_connection_app_bar = 'no_internet_connection_app_bar';
  static const about_app = 'about_app';
  static const about_app_content = 'about_app_content';
  static const no_camera_access_title = 'no_camera_access_title';
  static const no_camera_access_message = 'no_camera_access_message';
  static const dark_theme = 'dark_theme';
  static const show_chords = 'show_chords';
  static const font_size = 'font_size';
  static const settings = 'settings';
  static const song_book = 'song_book';
  static const search_song = 'search_song';
  static const song_or_author = 'song_or_author';
  static const search = 'search';
  static const save = 'save';
  static const saved_successfully = 'saved_successfully';
  static const songs = 'songs';
  static const downloaded = 'downloaded';
  static const delete = 'delete';
  static const add_new_song_by_search_and_download = 'add_new_song_by_search_and_download';
  static const or = 'or';
  static const new_song_by_scan_qr_code = 'new_song_by_scan_qr_code';
  static const playlists = 'playlists';
  static const edit = 'edit';
  static const create_new_playlist = 'create_new_playlist';
  static const edit_playlist = 'edit_playlist';
  static const new_playlist = 'new_playlist';
  static const create = 'create';
  static const name = 'name';
  static const search_headline = 'search_headline';
  static const share_by_qr_code = 'share_by_qr_code';
  static const qr_code_for_song_share = 'qr_code_for_song_share';
  static const song_settings = 'song_settings';
  static const play_audio = 'play_audio';
  static const select_playlist_or_create_new = 'select_playlist_or_create_new';
  static const add_some_songs_to_playlist = 'add_some_songs_to_playlist';
  static const add_song_by_qr_code_headline = 'add_song_by_qr_code_headline';
  static const no_data = 'no_data';
  static const no_songs_message = 'no_songs_message';
  static const invalid_input = 'invalid_input';
  static const invalid_input_message = 'invalid_input_message';

}
