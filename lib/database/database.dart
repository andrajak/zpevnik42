import 'dart:async';
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'package:zpevnik/model/playlist.dart';
import 'package:zpevnik/model/song.dart';
import 'package:zpevnik/model/playlist_song.dart';
import 'package:zpevnik/database/dao/playlist_dao.dart';
import 'package:zpevnik/database/dao/song_dao.dart';
import 'package:zpevnik/database/dao/playlist_song_dao.dart';

part 'database.g.dart';

@Database(version: 1, entities: [Song, Playlist, PlaylistSong])
abstract class AppDatabase extends FloorDatabase {
  static const databaseName = "app_database.db";

  SongDao get songDao;
  PlaylistDao get playlistDao;
  PlaylistSongDao get playlistSongDao;
}