// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  SongDao? _songDaoInstance;

  PlaylistDao? _playlistDaoInstance;

  PlaylistSongDao? _playlistSongDaoInstance;

  Future<sqflite.Database> open(
    String path,
    List<Migration> migrations, [
    Callback? callback,
  ]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `song` (`id` INTEGER, `idG` INTEGER, `title` TEXT, `url` TEXT, `albumArt` TEXT, `lyrics` TEXT, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `playlist` (`id` INTEGER, `name` TEXT, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `playlist_song` (`playlistId` INTEGER, `songId` INTEGER, FOREIGN KEY (`playlistId`) REFERENCES `playlist` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION, FOREIGN KEY (`songId`) REFERENCES `song` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION, PRIMARY KEY (`playlistId`, `songId`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  SongDao get songDao {
    return _songDaoInstance ??= _$SongDao(database, changeListener);
  }

  @override
  PlaylistDao get playlistDao {
    return _playlistDaoInstance ??= _$PlaylistDao(database, changeListener);
  }

  @override
  PlaylistSongDao get playlistSongDao {
    return _playlistSongDaoInstance ??=
        _$PlaylistSongDao(database, changeListener);
  }
}

class _$SongDao extends SongDao {
  _$SongDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database, changeListener),
        _songInsertionAdapter = InsertionAdapter(
            database,
            'song',
            (Song item) => <String, Object?>{
                  'id': item.id,
                  'idG': item.idG,
                  'title': item.title,
                  'url': item.url,
                  'albumArt': item.albumArt,
                  'lyrics': item.lyrics
                },
            changeListener),
        _songUpdateAdapter = UpdateAdapter(
            database,
            'song',
            ['id'],
            (Song item) => <String, Object?>{
                  'id': item.id,
                  'idG': item.idG,
                  'title': item.title,
                  'url': item.url,
                  'albumArt': item.albumArt,
                  'lyrics': item.lyrics
                },
            changeListener),
        _songDeletionAdapter = DeletionAdapter(
            database,
            'song',
            ['id'],
            (Song item) => <String, Object?>{
                  'id': item.id,
                  'idG': item.idG,
                  'title': item.title,
                  'url': item.url,
                  'albumArt': item.albumArt,
                  'lyrics': item.lyrics
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<Song> _songInsertionAdapter;

  final UpdateAdapter<Song> _songUpdateAdapter;

  final DeletionAdapter<Song> _songDeletionAdapter;

  @override
  Future<void> deleteRelatedPlaylistSongs(int songId) async {
    await _queryAdapter.queryNoReturn(
        'DELETE FROM playlist_song WHERE songId = ?1',
        arguments: [songId]);
  }

  @override
  Stream<List<Song>> watchSongs() {
    return _queryAdapter.queryListStream('SELECT * FROM song',
        mapper: (Map<String, Object?> row) => Song(
            id: row['id'] as int?,
            idG: row['idG'] as int?,
            title: row['title'] as String?,
            url: row['url'] as String?,
            albumArt: row['albumArt'] as String?,
            lyrics: row['lyrics'] as String?),
        queryableName: 'song',
        isView: false);
  }

  @override
  Future<List<Song>> findAllSongs() async {
    return _queryAdapter.queryList('SELECT * FROM song',
        mapper: (Map<String, Object?> row) => Song(
            id: row['id'] as int?,
            idG: row['idG'] as int?,
            title: row['title'] as String?,
            url: row['url'] as String?,
            albumArt: row['albumArt'] as String?,
            lyrics: row['lyrics'] as String?));
  }

  @override
  Stream<Song?> findSongById(int id) {
    return _queryAdapter.queryStream('SELECT * FROM song WHERE id = ?1',
        mapper: (Map<String, Object?> row) => Song(
            id: row['id'] as int?,
            idG: row['idG'] as int?,
            title: row['title'] as String?,
            url: row['url'] as String?,
            albumArt: row['albumArt'] as String?,
            lyrics: row['lyrics'] as String?),
        arguments: [id],
        queryableName: 'song',
        isView: false);
  }

  @override
  Future<void> insertSong(Song song) async {
    await _songInsertionAdapter.insert(song, OnConflictStrategy.abort);
  }

  @override
  Future<void> updateSong(Song song) async {
    await _songUpdateAdapter.update(song, OnConflictStrategy.abort);
  }

  @override
  Future<void> deleteSong(Song song) async {
    await _songDeletionAdapter.delete(song);
  }

  @override
  Future<void> deleteSongWithDependencies(Song song) async {
    if (database is sqflite.Transaction) {
      await super.deleteSongWithDependencies(song);
    } else {
      await (database as sqflite.Database)
          .transaction<void>((transaction) async {
        final transactionDatabase = _$AppDatabase(changeListener)
          ..database = transaction;
        await transactionDatabase.songDao.deleteSongWithDependencies(song);
      });
    }
  }
}

class _$PlaylistDao extends PlaylistDao {
  _$PlaylistDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database, changeListener),
        _playlistInsertionAdapter = InsertionAdapter(
            database,
            'playlist',
            (Playlist item) =>
                <String, Object?>{'id': item.id, 'name': item.name},
            changeListener),
        _playlistUpdateAdapter = UpdateAdapter(
            database,
            'playlist',
            ['id'],
            (Playlist item) =>
                <String, Object?>{'id': item.id, 'name': item.name},
            changeListener),
        _playlistDeletionAdapter = DeletionAdapter(
            database,
            'playlist',
            ['id'],
            (Playlist item) =>
                <String, Object?>{'id': item.id, 'name': item.name},
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<Playlist> _playlistInsertionAdapter;

  final UpdateAdapter<Playlist> _playlistUpdateAdapter;

  final DeletionAdapter<Playlist> _playlistDeletionAdapter;

  @override
  Future<void> deleteRelatedPlaylistSongs(int playlistId) async {
    await _queryAdapter.queryNoReturn(
        'DELETE FROM playlist_song WHERE playlistId = ?1',
        arguments: [playlistId]);
  }

  @override
  Stream<List<Playlist>> watchPlaylists() {
    return _queryAdapter.queryListStream('SELECT * FROM playlist',
        mapper: (Map<String, Object?> row) =>
            Playlist(id: row['id'] as int?, name: row['name'] as String?),
        queryableName: 'playlist',
        isView: false);
  }

  @override
  Future<List<Playlist>> findAllPlaylists() async {
    return _queryAdapter.queryList('SELECT * FROM playlist',
        mapper: (Map<String, Object?> row) =>
            Playlist(id: row['id'] as int?, name: row['name'] as String?));
  }

  @override
  Stream<Playlist?> findPlaylistById(int id) {
    return _queryAdapter.queryStream('SELECT * FROM playlist WHERE id = ?1',
        mapper: (Map<String, Object?> row) =>
            Playlist(id: row['id'] as int?, name: row['name'] as String?),
        arguments: [id],
        queryableName: 'playlist',
        isView: false);
  }

  @override
  Future<void> insertPlaylist(Playlist playlist) async {
    await _playlistInsertionAdapter.insert(playlist, OnConflictStrategy.abort);
  }

  @override
  Future<void> updatePlaylist(Playlist playlist) async {
    await _playlistUpdateAdapter.update(playlist, OnConflictStrategy.abort);
  }

  @override
  Future<void> deletePlaylist(Playlist playlist) async {
    await _playlistDeletionAdapter.delete(playlist);
  }

  @override
  Future<void> deletePlaylistWithDependencies(Playlist playlist) async {
    if (database is sqflite.Transaction) {
      await super.deletePlaylistWithDependencies(playlist);
    } else {
      await (database as sqflite.Database)
          .transaction<void>((transaction) async {
        final transactionDatabase = _$AppDatabase(changeListener)
          ..database = transaction;
        await transactionDatabase.playlistDao
            .deletePlaylistWithDependencies(playlist);
      });
    }
  }
}

class _$PlaylistSongDao extends PlaylistSongDao {
  _$PlaylistSongDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database, changeListener),
        _playlistSongInsertionAdapter = InsertionAdapter(
            database,
            'playlist_song',
            (PlaylistSong item) => <String, Object?>{
                  'playlistId': item.playlistId,
                  'songId': item.songId
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<PlaylistSong> _playlistSongInsertionAdapter;

  @override
  Stream<List<PlaylistSong>> watchPlaylistSong() {
    return _queryAdapter.queryListStream('SELECT * FROM playlist_song',
        mapper: (Map<String, Object?> row) => PlaylistSong(
            playlistId: row['playlistId'] as int?,
            songId: row['songId'] as int?),
        queryableName: 'playlist_song',
        isView: false);
  }

  @override
  Future<List<Song>> findSongsForPlaylist(int playlistId) async {
    return _queryAdapter.queryList(
        'SELECT * FROM song INNER JOIN playlist_song ON song.id = playlist_song.songId WHERE playlist_song.playlistId = ?1',
        mapper: (Map<String, Object?> row) => Song(id: row['id'] as int?, idG: row['idG'] as int?, title: row['title'] as String?, url: row['url'] as String?, albumArt: row['albumArt'] as String?, lyrics: row['lyrics'] as String?),
        arguments: [playlistId]);
  }

  @override
  Future<List<Playlist>> findPlaylistsForSong(int songId) async {
    return _queryAdapter.queryList(
        'SELECT * FROM playlist INNER JOIN playlist_song ON playlist.id = playlist_song.playlistId WHERE playlist_song.songId = ?1',
        mapper: (Map<String, Object?> row) => Playlist(id: row['id'] as int?, name: row['name'] as String?),
        arguments: [songId]);
  }

  @override
  Future<void> deletePlaylistSong(
    int playlistId,
    int songId,
  ) async {
    await _queryAdapter.queryNoReturn(
        'DELETE FROM playlist_song WHERE playlistId = ?1 AND songId = ?2',
        arguments: [playlistId, songId]);
  }

  @override
  Future<void> insertPlaylistSong(PlaylistSong playlistSong) async {
    await _playlistSongInsertionAdapter.insert(
        playlistSong, OnConflictStrategy.abort);
  }
}
