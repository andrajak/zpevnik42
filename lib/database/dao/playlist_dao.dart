import 'package:floor/floor.dart';

import 'package:zpevnik/model/playlist.dart';

@dao
abstract class PlaylistDao {
  @transaction
  Future<void> deletePlaylistWithDependencies(Playlist playlist) async {
    await deleteRelatedPlaylistSongs(playlist.id!);
    await deletePlaylist(playlist);
  }

  @Query('DELETE FROM playlist_song WHERE playlistId = :playlistId')
  Future<void> deleteRelatedPlaylistSongs(int playlistId);

  @Query('SELECT * FROM playlist')
  Stream<List<Playlist>> watchPlaylists();

  @Query('SELECT * FROM playlist')
  Future<List<Playlist>> findAllPlaylists();

  @Query('SELECT * FROM playlist WHERE id = :id')
  Stream<Playlist?> findPlaylistById(int id);

  @insert
  Future<void> insertPlaylist(Playlist playlist);

  @update
  Future<void> updatePlaylist(Playlist playlist);

  @delete
  Future<void> deletePlaylist(Playlist playlist);
}
