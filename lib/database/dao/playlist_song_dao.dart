import 'package:floor/floor.dart';
import 'package:zpevnik/model/playlist.dart';
import 'package:zpevnik/model/playlist_song.dart';
import 'package:zpevnik/model/song.dart';

@dao
abstract class PlaylistSongDao {
  @Query('SELECT * FROM playlist_song')
  Stream<List<PlaylistSong>> watchPlaylistSong();

  @insert
  Future<void> insertPlaylistSong(PlaylistSong playlistSong);

  @Query('SELECT * FROM song INNER JOIN playlist_song ON song.id = playlist_song.songId WHERE playlist_song.playlistId = :playlistId')
  Future<List<Song>> findSongsForPlaylist(int playlistId);

  @Query('SELECT * FROM playlist INNER JOIN playlist_song ON playlist.id = playlist_song.playlistId WHERE playlist_song.songId = :songId')
  Future<List<Playlist>> findPlaylistsForSong(int songId);

  @Query('DELETE FROM playlist_song WHERE playlistId = :playlistId AND songId = :songId')
  Future<void> deletePlaylistSong(int playlistId, int songId);
}
