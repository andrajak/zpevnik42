import 'package:floor/floor.dart';
import 'package:zpevnik/model/song.dart';

@dao
abstract class SongDao {
  @transaction
  Future<void> deleteSongWithDependencies(Song song) async {
    await deleteRelatedPlaylistSongs(song.id!);
    await deleteSong(song);
  }

  @Query('DELETE FROM playlist_song WHERE songId = :songId')
  Future<void> deleteRelatedPlaylistSongs(int songId);

  @Query('SELECT * FROM song')
  Stream<List<Song>> watchSongs();

  @Query('SELECT * FROM song')
  Future<List<Song>> findAllSongs();

  @Query('SELECT * FROM song WHERE id = :id')
  Stream<Song?> findSongById(int id);

  @insert
  Future<void> insertSong(Song song);

  @update
  Future<void> updateSong(Song song);

  @delete
  Future<void> deleteSong(Song song);
}
