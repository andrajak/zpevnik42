import 'package:flutter/material.dart';
import 'package:zpevnik/libraries/easy_localization/lib/src/easy_localization_app.dart';
import 'package:zpevnik/locator.dart';
import 'package:zpevnik/my_app.dart';
import 'package:zpevnik/service/session_manager.dart';


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await setupServices();
  await SessionManager.to.onAppInit();

  runApp(
    EasyLocalization(
      supportedLocales: InitUtils.supportedLocales,
      path: InitUtils.localizationPath,
      fallbackLocale: InitUtils.supportedLocales.first,
      useFallbackTranslations: true,
      child: MyApp(),
    ),
  );
}

class InitUtils {
  static String localizationPath = 'assets/translations';
  static List<Locale> supportedLocales = [
    const Locale('en'),
    const Locale('cs'),
  ];
}
