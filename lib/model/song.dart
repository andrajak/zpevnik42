import 'package:floor/floor.dart';
import 'package:json_annotation/json_annotation.dart';

part 'song.g.dart';

@JsonSerializable()
@Entity(tableName: 'song')
class Song {
  @primaryKey
  int? id;
  int? idG;
  String? title;
  String? url;
  String? albumArt;
  String? lyrics;

  Song({
    this.id,
    this.idG,
    this.title,
    this.url,
    this.albumArt,
    this.lyrics,
  });

  factory Song.fromJson(Map<String, dynamic> json) => _$SongFromJson(json);
  Map<String, dynamic> toJson() => _$SongToJson(this);

  factory Song.fromQrCode(String qrData) {
    final parts = qrData.split(qrCodeDelimiter);
    return parts.length == 2
        ? Song(
            title: parts[0],
            lyrics: parts[1],
          )
        : Song();
  }

  static const String qrCodeDelimiter = "&&&";

  @override
  String toString() {
    return title ?? 'Unknown';
  }
}
