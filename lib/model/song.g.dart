// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'song.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Song _$SongFromJson(Map<String, dynamic> json) => Song(
      id: json['id'] as int?,
      idG: json['idG'] as int?,
      title: json['title'] as String?,
      url: json['url'] as String?,
      albumArt: json['albumArt'] as String?,
      lyrics: json['lyrics'] as String?,
    );

Map<String, dynamic> _$SongToJson(Song instance) => <String, dynamic>{
      'id': instance.id,
      'idG': instance.idG,
      'title': instance.title,
      'url': instance.url,
      'albumArt': instance.albumArt,
      'lyrics': instance.lyrics,
    };
