import 'dart:core';

import 'package:flutter/material.dart';
import 'package:zpevnik/generated/locale_keys.g.dart';
import 'package:zpevnik/libraries/easy_localization/lib/src/public.dart';
import 'package:zpevnik/screens/home_screen.dart';
import 'package:zpevnik/screens/playlist_screen.dart';
import 'package:zpevnik/screens/search_screen.dart';
import 'package:zpevnik/screens/song_list_view_screen.dart';

enum NavigationMenuItemType {
  home,
  search,
  songs,
  playlists,
  notSpecified
}

class NavigationMenuItem {
  final String label;
  final String iconPath;
  final NavigationMenuItemType type;
  Widget screen;
  VoidCallback? onTap;

  NavigationMenuItem(
      this.label,
      this.iconPath,
      this.screen, {
        this.type = NavigationMenuItemType.notSpecified,
        this.onTap,
      });
}

class NavigationMenu {
  static List<NavigationMenuItem> itemsForMobile() {
    return [
      //NavigationMenuItem(tr(LocaleKeys.tab_bar_home), "assets/images/ic_24_home.png", HomeScreen(), type: NavigationMenuItemType.home),
      NavigationMenuItem("hledani", "assets/images/ic_24_home.png", const SearchScreen(), type: NavigationMenuItemType.search),
      NavigationMenuItem("songs", "assets/images/ic_24_file.png", const SongListViewScreen(), type: NavigationMenuItemType.songs),
      NavigationMenuItem("playlists", "assets/images/ic_24_file.png", const PlaylistScreen(), type: NavigationMenuItemType.playlists),
    ];
  }
}