import 'package:floor/floor.dart';
import 'package:zpevnik/model/playlist.dart';
import 'package:zpevnik/model/song.dart';

@Entity(
  tableName: 'playlist_song',
  primaryKeys: ['playlistId', 'songId'],
  foreignKeys: [
    ForeignKey(
      childColumns: ['playlistId'],
      parentColumns: ['id'],
      entity: Playlist,
      //onDelete: ForeignKeyAction.cascade,
    ),
    ForeignKey(
      childColumns: ['songId'],
      parentColumns: ['id'],
      entity: Song,
      //onDelete: ForeignKeyAction.cascade,
    ),
  ],
)
class PlaylistSong {
  int? playlistId;
  int? songId;

  PlaylistSong({
    this.playlistId,
    this.songId,
  });
}
