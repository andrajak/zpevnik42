import 'package:floor/floor.dart';
import 'package:json_annotation/json_annotation.dart';

part 'playlist.g.dart';

@JsonSerializable()
@Entity(tableName: 'playlist')
class Playlist {
  @primaryKey
  int? id;
  String? name;

  Playlist({
    this.id,
    this.name,
  });

  factory Playlist.fromJson(Map<String, dynamic> json) => _$PlaylistFromJson(json);
  Map<String, dynamic> toJson() => _$PlaylistToJson(this);
}
