import 'package:flutter/material.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:get/get.dart';
import 'package:zpevnik/libraries/easy_localization/lib/src/public_ext.dart';
import 'package:zpevnik/screens/main/main_screen.dart';
import 'package:zpevnik/service/session_manager.dart';
import 'my_app_theme_data.dart';

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  DateTime? appPausedAt;
  final MyAppThemeData myAppThemeData = MyAppThemeData();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void deactivate() {
    WidgetsBinding.instance.removeObserver(this);
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Obx(() {
        return GetMaterialApp(
          localizationsDelegates: [
            ...context.localizationDelegates,
            const LocaleNamesLocalizationsDelegate(),
          ],
          supportedLocales: context.supportedLocales,
          locale: context.locale,
          title: 'Zpevnik',
          themeMode: SessionManager.to.themeModeIndex.value,
          theme: myAppThemeData.themeData,
          darkTheme: myAppThemeData.darkThemeData,
          home: const MainScreen(),
        );
      }),
    );
  }

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.resumed:
        if (appPausedAt != null) {}
        appPausedAt = null;
        break;

      case AppLifecycleState.paused:
        appPausedAt = DateTime.now();
        break;

      case AppLifecycleState.detached:
        break;
      default:
        break;
    }
  }
}
