import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpevnik/network/genius_song_rest_client.dart';

abstract class BaseController extends FullLifeCycleController with FullLifeCycleMixin {
  Widget get progressWidget {
    return Center(
      child: CircularProgressIndicator(
        color: Get.theme.colorScheme.secondary,
      ),
    );
  }

  bool hasInternet() {
    return GeniusSongRestClient.to.hasNetworkConnection.isTrue;
  }

  @override
  void onResumed() {}

  @override
  void onPaused() {}

  @override
  void onInactive() {}

  @override
  void onDetached() {}
}
