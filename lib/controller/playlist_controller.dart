import 'package:get/get.dart';

import 'package:zpevnik/database/dao/playlist_dao.dart';
import 'package:zpevnik/database/database.dart';
import 'package:zpevnik/model/playlist.dart';
import 'package:zpevnik/controller/base_controller.dart';

class PlaylistController extends BaseController {
  static PlaylistController get to => Get.find();

  PlaylistController({
    required AppDatabase database,
  }) : _database = database;

  final AppDatabase _database;
  late PlaylistDao _playlistDao;

  @override
  void onInit() {
    super.onInit();
    _playlistDao = _database.playlistDao;

    final Stream<List<Playlist>> playlistStream = _playlistDao.watchPlaylists();

    playlists.bindStream(playlistStream);
  }

  final RxList<Playlist> playlists = RxList<Playlist>();

  Future<void> deletePlaylist(Playlist playlist) async {
    try {
      await _playlistDao.deletePlaylistWithDependencies(playlist);
    } catch (e) {
      print(e);
    }
  }

  Future<void> createPlaylist(Playlist playlist) async {
    try {
      await _playlistDao.insertPlaylist(playlist);
    } catch (e) {
      print(e);
    }
  }

  Future<void> updatePlaylist(Playlist playlist) async {
    try {
      await _playlistDao.updatePlaylist(playlist);
    } catch (e) {
      print(e);
    }
  }
}