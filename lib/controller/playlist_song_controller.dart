import 'package:get/get.dart';

import 'package:zpevnik/database/database.dart';
import 'package:zpevnik/controller/base_controller.dart';
import 'package:zpevnik/database/dao/playlist_song_dao.dart';
import 'package:zpevnik/model/playlist_song.dart';
import 'package:zpevnik/model/playlist.dart';
import 'package:zpevnik/model/song.dart';

class PlaylistSongController extends BaseController {
  static PlaylistSongController get to => Get.find();

  PlaylistSongController({
    required AppDatabase database,
  }) : _database = database;

  final AppDatabase _database;
  late PlaylistSongDao _playlistSongDao;

  @override
  void onInit() {
    super.onInit();
    _playlistSongDao = _database.playlistSongDao;

    final Stream<List<PlaylistSong>> playlistSongStream = _playlistSongDao.watchPlaylistSong();

    playlistsSongs.bindStream(playlistSongStream);
  }

  final RxList<PlaylistSong> playlistsSongs = RxList<PlaylistSong>();

  Future<void> deletePlaylistSong(int playlistId, int songId) async {
    await _playlistSongDao.deletePlaylistSong(playlistId, songId);
  }

  Future<void> createPlaylistSong(int playlistId, int songId) async {
    await _playlistSongDao.insertPlaylistSong(PlaylistSong(playlistId: playlistId, songId: songId));
  }

  Future<List<Song>> findSongsForPlaylist(int playlistId) async {
    return _playlistSongDao.findSongsForPlaylist(playlistId);
  }

  Future<List<Playlist>> findPlaylistsForSong(int songId) async {
    return _playlistSongDao.findPlaylistsForSong(songId);
  }

  int getSongCountForPlaylist(Playlist playlist) {
    return playlistsSongs.where((playlistSong) => playlistSong.playlistId == playlist.id).length;
  }
}