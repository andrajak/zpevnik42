import 'package:get/get.dart';

import 'package:zpevnik/database/database.dart';
import 'package:zpevnik/controller/base_controller.dart';
import 'package:zpevnik/database/dao/song_dao.dart';
import 'package:zpevnik/model/song.dart';

class SongController extends BaseController {
  static SongController get to => Get.find();

  SongController({
    required AppDatabase database,
  }) : _database = database;

  final AppDatabase _database;
  late SongDao _songDao;

  @override
  void onInit() {
    super.onInit();
    _songDao = _database.songDao;

    final Stream<List<Song>> songStream = _songDao.watchSongs();

    songs.bindStream(songStream);
  }

  final RxList<Song> songs = RxList<Song>();

  Future<void> createSong(Song song) async {
    try {
      await _songDao.insertSong(song);
    } catch (e) {
      print(e);
    }
  }

  Future<void> updateSong(Song song) async {
    try {
      await _songDao.updateSong(song);
    } catch (e) {
      print(e);
    }
  }

  Future<void> deleteSong(Song song) async {
    try {
      await _songDao.deleteSong(song);
    } catch (e) {
      print(e);
    }
  }

  Future<void> deleteSongWithDependencies(Song song) async {
    try {
      await _songDao.deleteSongWithDependencies(song);
    } catch (e) {
      print(e);
    }
  }

  Future<void> createSongFromQrCode(String qrData) async {
    try {
      final song = Song.fromQrCode(qrData);
      if (song.title != null && song.lyrics != null) {
        await _songDao.insertSong(song);
      }
    } catch (e) {
      print(e);
    }
  }

  SimpleSong? splitGeniusSongName(String input) {
    final lastIndex = input.lastIndexOf("by");
    if (lastIndex != -1) {
      final name = input.substring(0, lastIndex).trim();
      final author = input.substring(lastIndex + 2).trim();
      return SimpleSong(name: name, author: author);
    } else {
      return null;
    }
  }
}

class SimpleSong {
  String? name;
  String? author;

  SimpleSong({
    this.name,
    this.author,
  });
}